import React, { Component, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import Spinner from './components/UI/Loader/Loader';

import Home from './containers/Home/Home';
import Layout from './hoc/Layout/Layout';
import InternalLayout from './hoc/InternalLayout/InternalLayout';
import ProtectedRoute from './services/ProtectedRoute';
import Casting from './containers/Casting/Casting';
import Ondemand from './containers/Ondemand/Ondemand';


class App extends Component {
  render() {
    return (
      <div>
        <Suspense fallback={<div>Loading...</div>}>
          <Layout>
            <Switch>
              <Route path="/Home" exact component={Home} />  }
              {/* <Route path = "/Casting"  component = { Casting } />
              <Route path = "/Ondemand" component = { Ondemand } /> */}
              <ProtectedRoute path="/" component={InternalLayout} />
              <Route path="*" exact component={() => (<h1>404 Not Fount !</h1>)} />

            </Switch>
          </Layout>
        </Suspense>
        {/* ==========PAGE LOADER============== */}

        <Spinner />

        {/* =================================== */}
      </div>
    );
  }
}

export default App;
