import axios from 'axios';
import {messageService} from './services/subscriberService';
import { env } from './services/CommonFunction';

const instance = axios.create({
    baseURL : env('Urls')
});

instance.interceptors.request.use(request => {
    messageService.sendMessage(true);
    let token = localStorage.getItem('accessToken');
    if (token)  {
        request.headers.access_token = token;
    }   
    return request ;
},
error => {
    // console.log(error);
    messageService.sendMessage(false);
    return Promise.reject(error);
});

instance.interceptors.response.use(response => {
    messageService.sendMessage(false);
    return response ;
},
error => {
    messageService.sendMessage(false);
    // console.log(error);
    messageService.sendMessage(false);
    return Promise.reject(error);
});

instance.defaults.headers.common['content-language'] = 'en';

export default instance;