import React, { Component } from 'react';
import './AmazonIVSWorkaround.scss';
import { browserName } from "react-device-detect";
import { ivsSubscriber, seekVideoSubscriber, seekCompleteSubscriber, bufferingSubscriber, playerPlaySubscriber } from '../../services/subscriberService';


class OnDemandPlayer extends Component {

    state = {
        // playback_url: "https://d41b5ac60c77.us-east-1.playback.live-video.net/api/video/v1/us-east-1.771368300963.channel.uBrj3PuSRg0Y.m3u8"
        playback_url: "",
        ivsStatus: false,
        seekStatus: false,
        isSafari: false,
        isPausedOnce: false,
        isVideoPaused: false,
        isVideoStarted: false,
        totalSeconds: 0,
        isOndemand: false,
        PlayerState: window.IVSPlayer.PlayerState,
        PlayerEventType: window.IVSPlayer.PlayerEventType,
        player: window.IVSPlayer.create(),
        vid: null
    }

    componentDidMount = async () => {
        console.log("browserName", browserName);
        if (browserName === 'Safari') {
            this.setState({
                isSafari: true
            });
        }
        await this.setState({ vid: document.getElementById('video-player') })
        this.subscription = ivsSubscriber.getIvsData().subscribe(async (data) => {
            console.log(' -----filter Subscriber ------', data);
            await this.setState({
                ivsStatus: data.data.ivsStatus,
                playbackUrl: data.data.playbackUrl
            });
            this.createPlayer(data.data.ivsStatus);
        });
        this.seekSubscription = seekVideoSubscriber.get().subscribe(data => {
            console.log("seekSubscription", data);
            this.state.player.seekTo(data.data);
        });
        this.playerPlaySubscription = playerPlaySubscriber.get().subscribe(data => {
            if (data.data) {
                console.log(data.data);
                this.state.player.play();
                this.setState({ isVideoPaused: false });
            } else {
                if (!this.state.isPausedOnce) {
                    this.setState({ isPausedOnce: true })
                }
                this.setState({ isVideoPaused: true });
                this.state.player.pause();
            }
        });
    }

    componentDidUpdate = async () => {
        if (this.props.URL && this.props.URL.length && this.props.URL !== this.state.playback_url) {
            console.log("component did update");
            await this.setState({
                playback_url: this.props.URL,
                ivsStatus: true,
                isOndemand: this.props.ondemand
            });
            this.createPlayer(this.props.ivsStatus);
        }
        // console.log(this.props.totalSeconds);
        if (this.state.totalSeconds !== this.props.totalSeconds) {
            // console.log("seconds updated", this.props.totalSeconds);
            this.setState({
                totalSeconds: this.props.totalSeconds
            });
        }
    }

    componentWillUnmount = () => {
        // this.state.player.dispose();
        this.listner('removeEventListener');
        this.state.player.delete();
        this.setState({
            player: null,
            PlayerState: null,
            PlayerEventType: null,
        })
        this.subscription.unsubscribe();
        this.seekSubscription.unsubscribe();
        this.playerPlaySubscription.unsubscribe();
        window.location.reload(true);
    }

    listner = (status) => {
        const { PlayerState, PlayerEventType, player } = this.state;
        player[status](PlayerState.PLAYING, this.playerPlaying);
        player[status](PlayerState.ENDED, this.playerEnded);
        player[status](PlayerState.IDLE, this.playerIdle);
        player[status](PlayerState.READY, this.playerReady);
        player[status](PlayerState.BUFFERING, this.playerBuffering);
        player[status](PlayerEventType.ERROR, this.playerError);
        player[status](PlayerEventType.SEEK_COMPLETED, this.playerSeekCompleted);
        player[status](PlayerEventType.REBUFFERING, this.playerRebuffering);
    }

    createPlayer = (status) => {
        // console.log("Player Status", status);
        if (window.IVSPlayer.isPlayerSupported) {
            const { player, playback_url, vid } = this.state;
            player.attachHTMLVideoElement(vid);
            if (status) {
                this.listner('addEventListener');
                // Setup stream and play
                player.load(playback_url);
            } else {
                // console.log("Player Paused");
                player.pause();
            }
        }
    }

    playerPlaying = () => {
        const { isOndemand, player, seekStatus, totalSeconds, vid, isVideoStarted } = this.state;
        console.log("Player State - PLAYING");
        if (!seekStatus && isOndemand) {
            // console.log("totalSeconds", totalSeconds);
            player.seekTo(totalSeconds);
            if (!localStorage.getItem('videoPausedOnce') && localStorage.getItem('videoPaused') === "PAUSE") {
                // this.state.vid.pause();
                player.pause();
                localStorage.removeItem('videoPaused');
                localStorage.setItem('videoPausedOnce', 'true');
            }
            this.setState({
                seekStatus: true,
            });
            // if (!isVideoStarted) {
            //     vid.play();
            //     this.setState({
            //         isVideoStarted: true,
            //     }); 
            // }
        }
    }

    playerReady = () => {
        console.log("Player State - READY");
    }

    playerBuffering = (data) => {
        console.log("player buffering", data);
        bufferingSubscriber.send(true);
        // console.log("Player State - READY");
    }

    playerRebuffering = (data) => {
        console.log("player Rebuffering", data);
        // console.log("Player State - READY");
    }

    playerIdle = () => {
        const { isOndemand, player, playback_url } = this.state;
        console.log("Player State - IDLE", this.state.vid.paused);
        console.log("isVideoPaused", this.state.isVideoPaused);
        const ondemandData = { ...JSON.parse(localStorage.getItem('ondemandData')) };
        if (!isOndemand) {
            player.load(playback_url);
            // this.state.player.play();
        }
        if ((this.state.isSafari && !this.state.isPausedOnce && ondemandData.status !== "PAUSE") ||
            (isOndemand && !this.state.isVideoPaused)) {
            player.load(playback_url);
        }
    }

    playerEnded = async () => {
        const { player, playback_url } = this.state;
        // console.log("Player State - ENDED");
        await this.setState({
            seekStatus: false
        });
        player.load(playback_url);
    }

    playerError = async (err) => {
        const { player, playback_url, ivsStatus } = this.state;
        // console.warn("Player Event - ERROR:", err);
        await this.setState({
            seekStatus: false
        });
        if (ivsStatus) {
            player.load(playback_url);
        }
    }

    playerSeekCompleted = (data) => {
        console.log("playerSeekCompleted", data);
        seekCompleteSubscriber.send(Math.round(data));
    }

    render() {
        return (
            <React.Fragment>
                <div className="IvsVideoBox">
                    <video className="VideoTag"
                        id="video-player"
                        playsInline
                        autoPlay
                        // controls
                        height={300}
                    />
                </div>
                {!this.state.ivsStatus ?
                    <div className="backMessage">
                        <p className="streamMessage">On Demand Video is yet to start.</p>
                    </div> : null}

            </React.Fragment>
        )
    }
}

export default OnDemandPlayer;