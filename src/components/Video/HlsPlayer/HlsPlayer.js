import React, { Component } from 'react';
import '../AmazonIVSWorkaround.scss';
import { ivsSubscriber } from '../../../services/subscriberService';
import Hls from "hls.js";

class HlsPlayer extends Component {

    state = {
        // playback_url: "https://d41b5ac60c77.us-east-1.playback.live-video.net/api/video/v1/us-east-1.771368300963.channel.uBrj3PuSRg0Y.m3u8"
        playback_url: "",
        ivsStatus: false,
        seekStatus: false,
        totalSeconds: 0,
        config: {
            autoStartLoad: false,
            startPosition : -1,
            debug: false,
        },
        isOndemand: false,
        PlayerState: window.IVSPlayer.PlayerState,
        PlayerEventType: window.IVSPlayer.PlayerEventType,
        hls: null,
        vid: null
    }

    componentDidMount = async () => {

        await this.setState({ 
            vid: document.getElementById('video-player'),
            hls: new Hls(this.state.config) 
        })
        this.subscription = ivsSubscriber.getIvsData().subscribe( async (data) => {
            console.log(' -----filter Subscriber ------', data);
            await this.setState({
                ivsStatus: data.data.ivsStatus,
                playbackUrl: data.data.playbackUrl
            });
            this.createPlayer(data.data.ivsStatus);
        });
    }

    componentDidUpdate = async () => {
        // console.log(this.props)
        if (this.props.URL && this.props.URL.length && this.props.URL !== this.state.playback_url) {
            await this.setState({
                playback_url: this.props.URL,
                ivsStatus: true,
                isOndemand: this.props.ondemand
            })
            this.hlsPlayerCreate();
        }
        if(this.state.totalSeconds !== this.props.totalSeconds) {
            this.setState( prevState => ({
                totalSeconds: this.props.totalSeconds,
                config: {...prevState.config,startPosition: this.props.totalSeconds}
            }))
        }
    }

    componentWillUnmount = async () => {
        this.state.hls.stopLoad()
        this.state.hls.detachMedia();
        this.state.hls.destroy();
        this.setState({
            vid: null,
            videoSrc: null,
            hls: null
        })
        this.subscription.unsubscribe();
        setTimeout(() => {
            console.log(this.state);
        },10000);
    }

    onLevelLoaded = (event, data) => {
        console.log(data);
        var level_duration = data.details.targetduration;
        console.log(level_duration);
    }

    hlsPlayerCreate = () => {
        const {hls, playback_url, vid, totalSeconds, ivsStatus} = this.state;
        if(ivsStatus) {
            if(Hls.isSupported()) {
                // var config = 
                // const hls = new Hls(config);
                
                hls.attachMedia(vid);
                hls.on(Hls.Events.MEDIA_ATTACHED, function() {

                    console.log('MEDIA_ATTACHED');
                    hls.loadSource(playback_url);
                    hls.on(Hls.Events.MANIFEST_PARSED, function() {
                        console.log('MANIFEST_PARSED');
                        hls.startLoad(totalSeconds);
                        vid.play();
                    });
                });
                // hls.on(Hls.Events.LEVEL_LOADED, this.onLevelLoaded);
                // hls.on(Hls.Events.INIT_PTS_FOUND, function() {
                //     console.log('INIT_PTS_FOUND');
                // });
                hls.on(Hls.Events.STREAM_STATE_TRANSITION, function(event,data) {
                    // console.log('STREAM_STATE_TRANSITION',event,data);
                });
                // hls.on(Hls.Events.FRAG_LOADED, function() {
                //     console.log('FRAG_LOADED');
                // });
                // hls.on(Hls.Events.FRAG_LOAD_EMERGENCY_ABORTED, function() {
                //     console.log('FRAG_LOAD_EMERGENCY_ABORTED');
                // });
                // hls.on(Hls.Events.FRAG_LOAD_PROGRESS, function() {
                //     console.log('FRAG_LOAD_PROGRESS');
                // });
                // hls.on(Hls.Events.FRAG_LOADING, function() {
                //     console.log('FRAG_LOADING');
                // });
                // hls.on(Hls.Events.IDLE, function() {
                //     console.log('IDLE');
                // });
                // hls.on(Hls.Events.FRAG_CHANGED, function() {
                //     console.log('FRAG_CHANGED');
                // });
                // hls.on(Hls.Events.FRAG_BUFFERED, function() {
                //     console.log('FRAG_BUFFERED');
                // });
                // hls.on(Hls.Events.FRAG_DECRYPTED, function() {
                //     console.log('FRAG_DECRYPTED');
                // });
                // hls.on(Hls.Events.FRAG_PARSED, function() {
                //     console.log('FRAG_PARSED');
                // });
                hls.on(Hls.Events.DESTROYING, function() {
                    console.log('DESTROYING');
                });
                hls.on(Hls.Events.ERROR, function (event, data) {
                    console.log(event, data);
                    if (data.fatal) {
                        switch(data.type) {
                            case Hls.ErrorTypes.NETWORK_ERROR:
                                // try to recover network error
                                console.log("fatal network error encountered, try to recover");
                                // hls.stopLoad()
                                hls.startLoad(totalSeconds);
                                break;
                            case Hls.ErrorTypes.MEDIA_ERROR:
                                console.log("fatal media error encountered, try to recover");
                                hls.recoverMediaError();
                                break;
                            default:
                                // cannot recover
                                console.log("error");
                                hls.destroy();
                                break;
                        }
                    }
                });                
                // hls.on(Hls.Events.MEDIA_ATTACHED, function() {
                //     console.log('MEDIA_ATTACHED');
                // });
                // hls.on(Hls.Events.MEDIA_ATTACHED, function() {
                //     console.log('MEDIA_ATTACHED');
                // });
                // console.log(totalSeconds)
                // hls.startLoad(totalSeconds);
                // hls.on(Hls.Events.MANIFEST_PARSED, function() { });
                // hls.on(Hls.Events.MANIFEST_PARSED, function() { });
                // hls.startLoad(totalSeconds);
                // setTimeout(() => {
                //     console.log('enter1',vid);
                //     vid.play();
                // },1000)
                console.log('playback_url',playback_url);
                console.log('enter',vid);
            } else if (vid.canPlayType('application/vnd.apple.mpegurl')) {
                vid.addEventListener('loadedmetadata', function() {
                    //  vid.play();
                    vid.src = playback_url;
                });
            }

        }

    }

    render() {
        return (
            <React.Fragment>
                <div className="IvsVideoBox">
                    <video className="VideoTag"
                        id="video-player"
                        playsInline
                        autoPlay
                        controls
                        height={300}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default HlsPlayer;