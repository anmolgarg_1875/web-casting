import React, { Component } from 'react';
import './AmazonIVSWorkaround.scss';
import { ivsSubscriber } from '../../services/subscriberService';


class AmazonIVSWorkaround extends Component {

    state = {
        // playback_url: "https://d41b5ac60c77.us-east-1.playback.live-video.net/api/video/v1/us-east-1.771368300963.channel.uBrj3PuSRg0Y.m3u8"
        playback_url: "",
        ivsStatus: false,
        seekStatus: false,
        totalSeconds: 0,
        isOndemand: false,
        PlayerState: window.IVSPlayer.PlayerState,
        PlayerEventType: window.IVSPlayer.PlayerEventType,
        player: window.IVSPlayer.create()
    }

    componentDidMount() {
        console.log(this.props)
        this.subscription = ivsSubscriber.getIvsData().subscribe(data => {
            console.log(' -----filter Subscriber ------', data);
            this.setState({
                ivsStatus: data.data.ivsStatus,
                playbackUrl: data.data.playbackUrl
            });
            if (data.data.ivsStatus) {
                setTimeout(() => {
                    this.createPlayer(1);
                }, 1000);
            } else {
                setTimeout(() => {
                    this.createPlayer(0);
                }, 1000);
            }
        });
    }

    componentDidUpdate() {
        // console.log("this.props.totalSeconds", this.props.totalSeconds);
        if (this.props.URL !== "" && this.state.playback_url === "" && this.state.ivsStatus === false) {
            // const xyz = this.props.totalSeconds
            // document.getElementById('video-player').addEventListener('loadedmetadata', function () {
            //     console.log("Hello", xyz);
            //     this.currentTime = xyz;
            // }, false);
            this.setState({
                playback_url: this.props.URL,
                ivsStatus: true,
                totalSeconds: this.props.totalSeconds,
                isOndemand: this.props.ondemand
            })
            setTimeout(() => {
                if (this.props.ivsStatus) {
                    this.createPlayer(1);
                } else {
                    this.createPlayer(0);
                }
            }, 1000);
        }
    }

    componentWillUnmount() {
        this.state.player.delete();
        this.subscription.unsubscribe();
    }

    createPlayer = (status) => {
        console.log("Player Status", status);
        if (window.IVSPlayer.isPlayerSupported) {
            const { PlayerState, PlayerEventType, player } = this.state;
            player.attachHTMLVideoElement(document.getElementById('video-player'));
            if (status) {
                console.log("Inside Load", this.state.playback_url);
                player.setRebufferToLive(true);
                player.load(this.state.playback_url);
                player.addEventListener(PlayerState.PLAYING, () => {
                    console.log("Player State - PLAYING", player.isMuted());
                    if (!this.state.seekStatus && this.state.isOndemand) {
                        console.log("Player Seek to", this.state.totalSeconds, this.props.totalSeconds);
                        this.state.player.seekTo(this.props.totalSeconds);
                        this.setState({
                            seekStatus: true
                        });
                    }
                });
                player.addEventListener(PlayerState.ENDED, () => {
                    console.log("Player State - ENDED");
                    this.setState({
                        seekStatus: false
                    });
                    player.setRebufferToLive(true);
                    player.load(this.state.playback_url);
                });
                player.addEventListener(PlayerState.IDLE, () => {
                    console.log("Player State - IDLE");
                    if (!this.state.isOndemand) {
                        player.setRebufferToLive(true);
                        player.load(this.state.playback_url);
                    }
                });
                player.addEventListener(PlayerState.READY, () => {
                    console.log("Player State - READY");
                    player.play();
                });
                player.addEventListener(PlayerEventType.ERROR, () => {
                    console.log("Player State - ERROR");
                    this.setState({
                        seekStatus: false
                    });
                    if (this.state.ivsStatus) {
                        player.setRebufferToLive(true);
                        player.load(this.state.playback_url);
                    }
                });
                player.addEventListener(PlayerEventType.AUDIO_BLOCKED, () => {
                    console.log("PlayerEvent - AUDIO_BLOCKED: ", player.isMuted());
                });
                player.addEventListener(PlayerEventType.MUTED_CHANGED, () => {
                    console.log("PlayerEvent - MUTED_CHANGED: ", player.isMuted());
                });
                player.addEventListener(PlayerEventType.WORKER_ERROR, () => {
                    console.log("PlayerEvent - WORKER_ERROR: ");
                });
                player.addEventListener(PlayerEventType.PLAYBACK_BLOCKED, () => {
                    console.log("PlayerEvent - PLAYBACK_BLOCKED: ");
                });
            } else {
                console.log("Player Paused");
                player.pause();
            }
        }
    }

    playerPaused = (data) => {
        // console.log("Player Paused", data);        
        // this.myInterval = setInterval(() => {
        //     this.createPlayer(1);
        // }, 2000);
    }

    onCanPlay = (data) => {
        // console.log("data", data);
        // clearInterval(this.myInterval)
    }


    render() {

        return (
            <React.Fragment>
                <div className="IvsVideoBox">
                    <video className="VideoTag"
                        id="video-player"
                        playsInline
                        autoPlay
                        height={300}
                        onPlay={e => this.onCanPlay(e.target.readyState)}
                        onPause={e => this.playerPaused(e.target.readyState)}
                        onLoadedMetadata={e => console.log(e.target.readyState)}
                        onLoadStart={e => console.log(e.target.readyState)}
                        onLoad={e => console.log(e.target.readyState)}
                        onLoadedData={e => console.log(e.target.readyState)}
                        onCanPlay={e => this.onCanPlay(e.target.readyState)}
                        onCanPlayThrough={e => this.onCanPlay(e.target.readyState)}

                    />
                </div>
                {!this.state.ivsStatus ?
                    <div className="backMessage">
                        <p className="streamMessage">Live Streaming is yet to start.</p>
                    </div> : null}

            </React.Fragment>
        )
    }
}

export default AmazonIVSWorkaround;