import ActivityDoneImg from '../../assets/Images/activity-done.png';
import CoachIcon from '../../assets/Images/coach.png';
import HoursIcon from '../../assets/Images/hours.png';
import NewCoachIcon from '../../assets/Images/newCoach.png';
import NewMemberIcon from '../../assets/Images/newMember.png';
import PaymentIcon from '../../assets/Images/payment.png';
import ScheduleIcon from '../../assets/Images/schedule.png';
import MemberIcon from '../../assets/Images/member.png';
import MinMemberIcon from '../../assets/Images/minMember.png';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import calendarIcon from '../../assets/Images/calendarIcon.png';
import userDummy from '../../assets/Images/dummy.jpg';
import Group from '../../assets/Images/group.png';
import Info from '../../assets/Images/info.png';

import GymStudio from '../../assets/Images/sidebar_gym_studio.png';
import calorieIcon from '../../assets/Images/calorie.svg';
import timerIcon from '../../assets/Images/timer.svg';
import verified from '../../assets/Images/icn_verified.svg';
import logo from '../../assets/Images/logo.png';
import userCover from '../../assets/Images/userCover.png';
import Bitmap from '../../assets/Images/Bitmap.png';
import DefaultImage from '../../assets/Images/Bitmap.svg';
import check from '../../assets/Images/check.png';
import RequestUser from '../../assets/Images/request_user.png';
import RemoveUser from '../../assets/Images/removed_user.png';
import RejectUser from '../../assets/Images/reject_user.png';

// =================SIDEBAR ICONS========================
import Dashboard from '../../assets/Images/sidebar_dashboard.png';
import Schedule from '../../assets/Images/sidebar_schedule.png';
import Ongoing from '../../assets/Images/sidebar_onGoingActivity.png';
import Activity from '../../assets/Images/sidebar/my_activity.svg';
import Recorded from '../../assets/Images/sidebar/recordedActivity.svg';
import Messages from '../../assets/Images/sidebar/messages.svg';
import Coaches from '../../assets/Images/sidebar/coaches.svg';
// import Members from '../../assets/Images/sidebar/members.svg';
import Members from '../../assets/Images/sidebar_members.png';
import Payments from '../../assets/Images/sidebar/payments.svg';
import Setting from '../../assets/Images/sidebar/settings.svg';
// =========================================================

// ====================HEADER  ICONS =======================
import PlayIcon from '../../assets/Images/header_play.png';
import StopIcon from '../../assets/Images/header_stop.png';
// =========================================================

// ====================PROFILE DROPDOWN ICONS ==============
import accountDetails from '../../assets/Images/profileDropdown/icn_account_detail.svg';
import agreements from '../../assets/Images/profileDropdown/icn_agreements.svg';
import privacyPolicy from '../../assets/Images/profileDropdown/icn_privacy_policy.svg';
import streamPlaceholder from '../../assets/Images/Artboard.svg';
import streamPlaceholder1 from '../../assets/Images/Artboard-1.svg';
import usersList from '../../assets/Images/usersList.svg';
import helpCenter from '../../assets/Images/profileDropdown/helpCenter.svg';
// =========================================================

// ==================== ZYM PROFILE ==============
import zym from '../../assets/profile/zym.png';
import zym1 from '../../assets/profile/zym1.png';
// =========================================================

// ==================== SPECIALISIMS ==============
import Running from '../../assets/Images/Running.png';
import Dance from '../../assets/Images/Dance.png';
import Postnatal from '../../assets/Images/Postnatal.png';
import Sports from '../../assets/Images/Sports.png';
import Stretch from '../../assets/Images/Stretch.png';
import Strength from '../../assets/Images/Strength.png';
import Prenatal from '../../assets/Images/Prenatal.png';
import Motivation from '../../assets/Images/Motivation.png';
import Pilates from '../../assets/Images/Pilates.png';
import Boxing from '../../assets/Images/Boxing.png';
import Spinning from '../../assets/Images/Spinning.png';
import Medication from '../../assets/Images/Medication.png';
import Bootcamp from '../../assets/Images/Bootcamp.png';
import Core from '../../assets/Images/Core.png';
import Barre from '../../assets/Images/Barre.png';
import Rowing from '../../assets/Images/Rowing.png';
import Martial from '../../assets/Images/Martial.png';
import Elliptical from '../../assets/Images/Elliptical.png';
import Yoga from '../../assets/Images/Yoga.png';
import checkImage from '../../assets/Images/checkImage.png';
import rankIcon from '../../assets/Images/rankIcon.png';
import redVideoIcon from '../../assets/Images/redVideoIcon.png';
import cameraFilled from '../../assets/Images/cameraFilled.svg';
import cameraBlank from '../../assets/Images/cameraBlank.svg';
import fullScreen from '../../assets/Images/fullScreen1.png';
import exitFullscreen from '../../assets/Images/exitFullscreen.png';
import scanGlobe from '../../assets/Images/scanGlobe.png';
import scanWifi from '../../assets/Images/scanWifi.png';
import caloriesIcon from '../../assets/Images/caloriesIcon.png';
import calorieIcon2x from '../../assets/Images/calorieIcon2x.png';
import backgroundStats from '../../assets/Images/statsBoxImage2.png';
// =========================================================

import blackImage from '../../assets/Images/black.png';
import seekBack from '../../assets/Images/seekBack.png';
import seekForward from '../../assets/Images/seekForward.png';
import videoPause from '../../assets/Images/videoPause.png';
import videoPlay from '../../assets/Images/videoPlay.png';
import volumeMute from '../../assets/Images/volumeMute.png';
import volumeUp from '../../assets/Images/volumeUp.png';
import coach2 from '../../assets/Images/coach2.png';
import sendMessage from '../../assets/Images/sendMessage.png';
import everyone from '../../assets/Images/everyone.png';
import owner from '../../assets/Images/owner.png';
import chatIcon from '../../assets/Images/chatIcon.png';



const selectIcon = (icon) => {
    switch (icon) {
        case 'Running':
            return Running;
        case 'Dance':
            return Dance;
        case 'Sports':
            return Postnatal;
        case 'Spinning':
            return Sports;
        case 'Outdoors':
            return Stretch;
        case 'HIIT':
            return Strength;
        case 'Prenatal':
            return Prenatal;
        case 'Motivation':
            return Motivation;
        case 'Pilates':
            return Pilates;
        case 'Boxing':
            return Boxing;
        case 'Cycling':
            return Spinning;
        case 'Medication':
            return Medication;
        case 'Bootcamp':
            return Bootcamp;
        case 'Core':
            return Core;
        case 'Barre':
            return Barre;
        case 'Rowing':
            return Rowing;
        case 'userCover':
            return userCover;
        case 'Martial Arts':
            return Martial;
        case 'Elliptical':
            return Elliptical;
        case 'Yoga':
            return Yoga;
        case 'info':
            return Info;
        case 'zym':
            return zym;
        case 'zym1':
            return zym1;
        case 'requestUser':
            return RequestUser;
        case 'removeUser':
            return RemoveUser;
        case 'rejectUser':
            return RejectUser;
        case 'check':
            return check;
        case 'calories':
            return calorieIcon;
        case 'DefaultImage':
            return DefaultImage;
        case 'heart':
            return faHeart;
        case 'group':
            return Group;
        case "activity":
            return ActivityDoneImg;
        case "coach":
            return CoachIcon;
        case "newCoach":
            return NewCoachIcon;
        case "newMember":
            return NewMemberIcon;
        case "payment":
            return PaymentIcon;
        case "hours":
            return HoursIcon;
        case "minMember":
            return MinMemberIcon;
        case "member":
            return MemberIcon;
        case "schedule":
            return ScheduleIcon;
        case "calender":
            return calendarIcon;
        case "sidebar_dashboard":
            return Dashboard;
        case "sidebar_schedule":
            return Schedule;
        case "sidebar_ongoing":
            return Ongoing;
        case "sidebar_activity":
            return Activity;
        case "sidebar_recorded":
            return Recorded;
        case "Bitmap":
            return Bitmap;
        case "sidebar_messages":
            return Messages;
        case "sidebar_coaches":
            return Coaches;
        case "sidebar_members":
            return Members;
        case "sidebar_payment":
            return Payments;
        case "sidebar_setting":
            return Setting;
        case "sidebar_gym_studio":
            return GymStudio;
        case "header_play":
            return PlayIcon;
        case "header_pause":
            return PlayIcon;
        case "header_stop":
            return StopIcon;
        case "timer":
            return faHeart;
        case "calorieIcon":
            return calorieIcon;
        case "timerIcon":
            return timerIcon;
        case "userDummy":
            return userDummy;
        case "verified":
            return verified;
        case "logo":
            return logo;
        case "checkImage":
            return checkImage;
        case "agreements":
            return agreements;
        case "accountDetails":
            return accountDetails;
        case "helpCenter":
            return helpCenter;
        case "privacyPolicy":
            return privacyPolicy;
        case "streamPlaceholder":
            return streamPlaceholder;
        case "streamPlaceholder1":
            return streamPlaceholder1;
        case "black":
            return blackImage;
        case "rankIcon":
            return rankIcon;
        case "redVideoIcon":
            return redVideoIcon;
        case "cameraFilled":
            return cameraFilled;
        case "cameraBlank":
            return cameraBlank;
        case "fullScreen":
            return fullScreen;
        case "exitFullscreen":
            return exitFullscreen;
        case "scanGlobe":
            return scanGlobe;
        case "scanWifi":
            return scanWifi;
        case "seekBack":
            return seekBack;
        case "seekForward":
            return seekForward;
        case "videoPause":
            return videoPause;
        case "videoPlay":
            return videoPlay;
        case "volumeMute":
            return volumeMute;
        case "volumeUp":
            return volumeUp;
        case "usersList":
            return usersList;
        case "caloriesIcon":
            return caloriesIcon;
        case "calorieIcon2x":
            return calorieIcon2x;
        case "backgroundStats":
            return backgroundStats;
        case "coach2":
            return coach2;
        case "everyone":
            return everyone;
        case "sendMessage":
            return sendMessage;
        case "chatIcon":
            return chatIcon;
        case "owner":
            return owner;
        default:
            return ActivityDoneImg;
    }
}

const colorSelector = (heartRate) => {
    switch (true) {
        case (heartRate > 0 && heartRate < 60):
            return "#507FB1";
        case (heartRate >= 60 && heartRate < 70):
            return "#55BA6B";
        case (heartRate >= 70 && heartRate < 80):
            return "#D6C345";
        case (heartRate >= 80 && heartRate < 90):
            return "#CF832D";
        case (heartRate >= 90):
            return "#E52353";
        default:
            // return "#ffffff";
            return "#808080";
    }
}

const textColorSelector = (heartRate) => {
    switch (true) {
        case (heartRate > 0 && heartRate < 60):
            return "#507FB1";
        case (heartRate >= 60 && heartRate < 70):
            return "#55BA6B";
        case (heartRate >= 70 && heartRate < 80):
            return "#D6C345";
        case (heartRate >= 80 && heartRate < 90):
            return "#CF832D";
        case (heartRate >= 90):
            return "#E52353";
        default:
            return "#ffffff";
    }
}




export {
    selectIcon,
    colorSelector,
    textColorSelector
};