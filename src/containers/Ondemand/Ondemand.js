import React, { Component } from 'react';
import IO from 'socket.io-client/dist/socket.io.js';
import { env } from '../../services/CommonFunction';
import axios from '../../axios';
import { throttle } from 'lodash';
import '../Casting/Casting.scss';
import { colorSelector, selectIcon } from '../../components/UI/selector';
import PlayStream from '../Casting/MemberStream/MemberStream';
import ApiURL from '../../services/ApiURL';
import AmazonIVSWorkaround from '../../components/Video/AmazonIVSWorkaround';
import BoardUser from '../Casting/BoardUser/BoardUser';
import UserStats from '../Casting/UserStats/UserStats';
import UserVideo from '../Casting/UserVideo/UserVideo';
import { Spring } from 'react-spring/renderprops'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faChevronLeft, faChevronRight, faVolumeUp, faRedoAlt,
    faVolumeMute, faChevronUp, faChevronDown, faUndo, faPlayCircle, faPauseCircle
} from '@fortawesome/free-solid-svg-icons';
import HlsPlayer from '../../components/Video/HlsPlayer/HlsPlayer';
import OnDemandPlayer from '../../components/Video/OnDemandPlayer';
import { seekVideoSubscriber, seekCompleteSubscriber, bufferingSubscriber, playerPlaySubscriber } from '../../services/subscriberService';
import { Range, getTrackBackground } from "react-range";
import MemberList from '../Casting/memberList/memberList';
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';



class Ondemand extends Component {

    constructor() {
        super();
        this.timer = null;
        this.myInterval = null;
        this.pollingInterval = null;
    }

    state = {
        STEP: 1,
        MIN: 0,
        MAX: 0,
        isInternetConnected: null,
        disconnectTime: 0,
        workoutData: {},
        ondemandData: {},
        onDemandControls: false,
        playerBuffering: false,
        totalMembers: 0,
        totalSeconds: 0,
        vid: null,
        playerPlayStatus: true,
        fullScreen: false,
        toggleDiv: {
            list: false,
            stats: false,
            video: false,
        },
        firstTaggedEmit: false,
        friendsWidth: 300,
        mute: false,
        boardWidth: 350,
        new_user: {},
        values: [1],
        selfData: {},
        usersInActivity: [],
        clubbedUsers: [],
        segmentDurations: [],
        seg_total_width: '0%',
        seg_total_duration: 0,
        showSeekBar: false,
        is_WorkoutBuilder_Available: false,
        cur_segment_data: {},
        cur_exercise_data: {},
        segment_duration_max: null,
        segment_duration_prev_max: null,
        socket: IO(env('OnDemandSocket') + localStorage.getItem('accessToken'), { transports: ['websocket'] }),
    }

    componentDidMount = async () => {
        let startTime = 0;
        let isSegmentSet = false;
        const currentTime = Math.round(new Date().getTime() / 1000);
        const ondemandData = { ...JSON.parse(localStorage.getItem('ondemandData')) };
        const workoutData = { ...JSON.parse(localStorage.getItem('workoutData')) };
        await this.setState({ vid: document.getElementById('video-player') });

        if (localStorage.getItem('startTime')) {
            startTime = parseInt(localStorage.getItem('startTime'));
        } else {
            startTime = Math.round((new Date().getTime() - (ondemandData.time_elapsed * 1000)) / 1000);
            localStorage.setItem('startTime', startTime);
        }
        this.state.socket.on('connect', this.socket_connected);
        this.state.socket.on('disconnect', this.socket_disconnected);
        this.setState({
            workoutData: { ...JSON.parse(localStorage.getItem('workoutData')) },
            ondemandData: { ...ondemandData },
            totalSeconds: localStorage.getItem('currentTime') ? parseInt(localStorage.getItem('currentTime')) : ondemandData.time_elapsed ? (Math.round(currentTime) - Math.round(startTime)) + 1 : 0,
            values: [Math.round(ondemandData.time_elapsed ? (Math.round(currentTime) - Math.round(startTime)) + 1 : 0)]
        })
        setTimeout(() => {
            this.getData();
        }, 1000);
        this.mouseMoved = throttle(this.mouseMoved, 3000);
        this.updateSegmentArray = throttle(this.updateSegmentArray, 200);
        this.seekCompleteSubscription = seekCompleteSubscriber.get().subscribe(data => {
            console.log("seekComplete", data, this.state.vid.duration);
            if (this.state.vid.duration !== 'NaN' && !isSegmentSet
                && workoutData.workout_builder && workoutData.workout_builder.length) {
                isSegmentSet = true;
                if (!localStorage.getItem('videoPausedOnce') && ondemandData.status === "PAUSE") {
                    localStorage.setItem('videoPausedOnce', 'true');
                    this.videoControl('pause');
                }
                this.setState({
                    MAX: Math.round(this.state.vid.duration),
                    is_WorkoutBuilder_Available: true
                });
                this.setSegmentData([...workoutData.workout_builder], Math.round(this.state.vid.duration));
            } else if (this.state.vid.duration !== 'NaN' && !isSegmentSet) {
                isSegmentSet = true;
                if (!localStorage.getItem('videoPausedOnce') && ondemandData.status === "PAUSE") {
                    localStorage.setItem('videoPausedOnce', 'true');
                    this.videoControl('pause');
                }
                this.setState({
                    is_WorkoutBuilder_Available: false,
                    MAX: Math.round(this.state.vid.duration),
                    seg_total_width: '100',
                    seg_total_duration: Math.round(this.state.vid.duration),
                    showSeekBar: true
                });
            }
            if (this.state.playerBuffering) {
                //consolele.log("Timer Started");
                clearInterval(this.myInterval);
                this.startTimer();
                this.setState({
                    playerBuffering: false
                });
            }
            this.setState({
                totalSeconds: data.data
            })
        });
        this.bufferingSubscription = bufferingSubscriber.get().subscribe(data => {
            ////consolele.log("bufferingSubscriber", data.data);
            if ((Math.round(this.state.vid.duration) - this.state.totalSeconds) === 2) {
                ////consolele.log("Time Matched");
                this.state.vid.pause();
                clearInterval(this.myInterval);
                this.stopActivity();
            }
            clearInterval(this.myInterval);
            this.setState({
                playerBuffering: true
            });
        });
    }

    setSegmentData = (workout_builder, workout_duration) => {
        if (this.state.is_WorkoutBuilder_Available) {
            let seg_total_width = 0;
            let seg_total_duration = 0;
            let seg_prev_total_duration = 0;
            workout_builder.forEach(el => {
                el['segment_duration_in_seconds'] = el.segment_duration_in_seconds ? el.segment_duration_in_seconds : el.segment_duration_in_ms;
                seg_total_width = Math.round((el.segment_duration_in_seconds / workout_duration) * 100) + seg_total_width;
                if (seg_total_duration) {
                    seg_prev_total_duration = seg_total_duration;
                }
                seg_total_duration = el.segment_duration_in_seconds + seg_total_duration;
                if (seg_total_duration < workout_duration) {
                    el['width'] = Math.round((el.segment_duration_in_seconds / workout_duration) * 100) + '%';
                } else if (workout_duration > seg_prev_total_duration) {
                    el['width'] = Math.round(((workout_duration - seg_prev_total_duration) / workout_duration) * 100) + '%';
                    el['segment_duration_in_seconds'] = workout_duration - seg_prev_total_duration;
                } else {
                    el['width'] = 0
                }
            });

            this.updateSegmentArray([...workout_builder.filter(el => el.width)], this.state.totalSeconds);

            this.setState({
                seg_total_width: seg_total_duration <= workout_duration ? seg_total_width : '100',
                seg_total_duration: seg_total_duration
            });
            setTimeout(() => {
                this.setState({
                    showSeekBar: true
                });
            }, 100);
        }
    }

    updateSegmentArray = (segment_array, totalSeconds) => {
        if (this.state.is_WorkoutBuilder_Available) {
            let prevMax = null;
            let segment_duration_max = null;
            let segment_duration_prev_max = null;

            const cur_segment_data = segment_array.find(el =>
                totalSeconds <= el.combinedMax && ((el.combinedMax - el.segment_duration_in_seconds) <= totalSeconds));


            const cur_exercise_data = cur_segment_data !== undefined && cur_segment_data.exercises.length
                ? cur_segment_data.exercises
                    .find(ed => totalSeconds < ed.combinedExerciseMax) : {};

            segment_array.forEach(el => {
                ////////consolele.log("inside loop ");
                let prevExerciseMax = null;
                let ex_total_duration = 0;
                let ex_prev_total_duration = 0;
                el.exercises.forEach(ed => {
                    if (ex_total_duration) {
                        ex_prev_total_duration = ex_total_duration;
                    }
                    ex_total_duration = ed.exercise_duration_in_seconds + ex_total_duration;
                    if (ex_total_duration < el.segment_duration_in_seconds) {
                        ed['exercise_duration_in_seconds'] = ed.exercise_duration_in_seconds;
                    } else if (el.segment_duration_in_seconds > ex_prev_total_duration) {
                        ed['exercise_duration_in_seconds'] = el.segment_duration_in_seconds - ex_prev_total_duration;
                    } else {
                        ed['exercise_duration_in_seconds'] = 0;
                    }
                });
                el.exercises.forEach(ed => {
                    if (prevExerciseMax === null) {
                        ed['combinedExerciseMax'] = prevMax === null ?
                            ed.exercise_duration_in_seconds : ed.exercise_duration_in_seconds + prevMax;
                        prevExerciseMax = prevMax === null ?
                            ed.exercise_duration_in_seconds : ed.exercise_duration_in_seconds + prevMax;
                    } else {
                        ed['combinedExerciseMax'] = ed.exercise_duration_in_seconds + prevExerciseMax;
                        prevExerciseMax = ed.exercise_duration_in_seconds + prevExerciseMax;
                    }
                });
                if (prevMax === null) {
                    el['combinedMax'] = el.segment_duration_in_seconds;
                    prevMax = el.segment_duration_in_seconds;
                } else {
                    el['combinedMax'] = el.segment_duration_in_seconds + prevMax;
                    prevMax = el.segment_duration_in_seconds + prevMax;
                }
                if (totalSeconds <= el.combinedMax &&
                    segment_duration_max === null) {
                    el['values'] = [segment_duration_prev_max === null ? totalSeconds :
                        totalSeconds - (el.combinedMax - el.segment_duration_in_seconds)]
                    segment_duration_max = el.segment_duration_in_seconds;
                    this.setState({
                        segment_duration_max: el.segment_duration_in_seconds
                    });
                } else if (totalSeconds >= el.combinedMax) {
                    el['values'] = [el.segment_duration_in_seconds];
                    segment_duration_prev_max = el.segment_duration_in_seconds;
                    this.setState({
                        segment_duration_prev_max: el.segment_duration_in_seconds
                    });
                }
                else {
                    el['values'] = [0];
                }
                el['MIN'] = 0;
                el['MAX'] = el.segment_duration_in_seconds;
                el['STEP'] = 1;

            });
            this.setState({
                segmentDurations: [...segment_array],
                cur_segment_data: { ...cur_segment_data },
                cur_exercise_data: { ...cur_exercise_data }
            });
        }
    }

    /* View in fullscreen */
    openFullscreen = () => {
        var elem = document.documentElement;
        if (elem.requestFullscreen) {
            ////////////consolele.log("Hello Chrme");
            elem.requestFullscreen();
        } else if (elem.webkitRequestFullscreen) { /* Safari */
            ////////////consolele.log("Hello Safari");
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE11 */
            ////////////consolele.log("Hello IE11");
            elem.msRequestFullscreen();
        }
        this.setState({
            fullScreen: true
        })

    }

    /* Close fullscreen */
    closeFullscreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) { /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE11 */
            document.msExitFullscreen();
        }
        this.setState({
            fullScreen: false
        })
    }


    componentWillUnmount() {
        const data = {
            activity_id: this.state.ondemandData.activity_id,
            user_activity_session_id: this.state.ondemandData.user_activity_session_id
        }
        this.state.socket.emit("unsubscribe_activity", data, (obj) => {
            //////////consolele.log(obj);
            this.state.socket.disconnect();
        });
        this.seekCompleteSubscription.unsubscribe();
        this.bufferingSubscription.unsubscribe();
        localStorage.removeItem('currentTime');
        clearInterval(this.pollingInterval);
        clearInterval(this.myInterval);
    }

    socket_connected = () => {
        //////////consolele.log("connected");
        const ondemandData = { ...JSON.parse(localStorage.getItem('ondemandData')) };
        this.socketEmitHandler({
            activity_id: ondemandData.activity_id,
            user_activity_session_id: ondemandData.user_activity_session_id
        });
    }

    socket_disconnected = (data) => {
        //////////consolele.log("Disconnected", data);
        this.setState({
            isInternetConnected: false,
            disconnectTime: this.state.totalSeconds
        });
        clearInterval(this.pollingInterval);
    }

    socketEmitHandler = (data) => {
        this.state.socket.emit("web_workout_sync_request", data, (obj) => {
            //////////consolele.log("web_workout_sync_request", obj);
            if (obj.user_status_in_activity === 'ENDED' ||
                obj.web_preview === 'false') {
                this.end_activity();
            } else {
                this.setState({
                    isInternetConnected: true,
                    disconnectTime: 0
                });
                this.activityPolling();
                if (this.state.firstTaggedEmit) {
                    this.syncSocketEmitHandler({
                        user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                        web_socket_id: this.state.socket.id
                    });
                }
            }
        });
    };

    syncSocketEmitHandler = (data) => {
        this.state.socket.emit("sync_tagged_videos", data, (obj) => {
        });
    }

    checkLength = (data) => (
        data < 10 ? '0' + data : data
    )

    convertTime = (totalSeconds, type = ':') => {
        const second = totalSeconds % 60;
        const minute = parseInt((totalSeconds) / 60, 10);
        const minutes = parseInt((minute) % 60, 10);
        const hour = parseInt((minute) / 60, 10);

        let time = '';
        if (hour > 0) {
            time += this.checkLength(hour) + type;
        }
        time += this.checkLength(minutes) + type;
        time += this.checkLength(second);
        return time;
    }

    stopActivity = () => {
        const obj = {
            activity_id: this.state.ondemandData.activity_id,
            user_activity_session_id: this.state.ondemandData.user_activity_session_id
        }
        axios.post(ApiURL._endOndemand, obj).then(res => {
        });
        const data = {
            user_activity_session_id: this.state.ondemandData.user_activity_session_id,
            status: 'ENDED'
        }
        setTimeout(() => {
            this.state.socket.emit('ondemand_activity_sync', data, (res) => {
                this.end_activity();
            });
        }, 500);
    }

    activityPolling = () => {
        this.pollingInterval = setInterval(() => {
            const data = {
                user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                status: 'PROGRESS',
                current_duration: this.state.totalSeconds
            }
            //////////consolele.log("Polling", data);
            this.state.socket.emit('ondemand_activity_sync', data, (res) => {
                //////////consolele.log("Polling");
            });
        }, 10000)
    }

    startTimer = () => {
        this.myInterval = setInterval(() => {
            if (!this.state.isInternetConnected) {
                //////////consolele.log("disconnect counter", this.state.totalSeconds);
                // if ((this.state.totalSeconds - this.state.disconnectTime) > 9) {
                //     //////////consolele.log("Logout", this.state.totalSeconds);
                //     this.end_activity();
                // }

            }
            if ((Math.round(this.state.vid.duration) - this.state.totalSeconds) === 2) {
                ////consolele.log("Time Matched");
                this.state.vid.pause();
                clearInterval(this.myInterval);
                this.stopActivity();
            } else {
                let array = [];
                localStorage.setItem("currentTime", Math.round(this.state.vid.currentTime));
                if (this.state.is_WorkoutBuilder_Available) {
                    array = [...this.state.segmentDurations];
                    array.forEach(el => {
                        if (this.state.totalSeconds <= el.combinedMax && ((el.combinedMax - el.segment_duration_in_seconds) <= this.state.totalSeconds)) {
                            el['values'] = [(this.state.totalSeconds - (el.combinedMax - el.segment_duration_in_seconds)) + 1];
                            this.setState({
                                cur_segment_data: { ...el },
                                cur_exercise_data: el.exercises
                                    .find(ed => this.state.totalSeconds < ed.combinedExerciseMax)
                            });
                        }
                    });
                }
                ////consolele.log(Math.round(this.state.vid.duration), Math.round(this.state.vid.currentTime));
                //consolele.log("this.state.vid.paused", this.state.vid.paused);
                // if (this.state.vid.paused) {
                //     this.videoControl('pause');
                // }
                this.setState({
                    totalSeconds: Math.round(this.state.vid.currentTime),
                    values: [Math.round(this.state.vid.currentTime)],
                    segmentDurations: this.state.is_WorkoutBuilder_Available ? [...array] : []
                });
            }
            if (this.state.vid !== undefined && this.state.mute !== this.state.vid.muted) {
                this.setState({ mute: this.state.vid.muted });
            }
        }, 1000)
    }

    end_activity = (data) => {
        localStorage.removeItem('ondemandData');
        localStorage.removeItem('startTime');
        localStorage.removeItem('workoutData');
        localStorage.removeItem('accessToken');
        localStorage.removeItem('videoPausedOnce')
        localStorage.removeItem('currentTime');
        this.props.history.push('/Home');
    }

    self_heartbeat = (data) => {
        //////////consolele.log("self_heartbeat", data);
        if (this.state.playerPlayStatus) {
            this.setState(prevState => ({
                selfData: { ...data },
                usersInActivity: prevState.usersInActivity.map(el =>
                    el.user_activity_session_id === data.user_activity_session_id ? {
                        ...el,
                        calories_burnt: data.calories_burnt ? Math.round(data.calories_burnt) : 0,
                        color: colorSelector(data.max_heart_rate_percent ? Math.round(data.max_heart_rate_percent) : 0),
                        heartrate: data.heartrates.length ? data.heartrates[data.heartrates.length - 1].heart_rate_at_the_instant : 0,
                        maxHeartRate: Math.round(208 - (0.7 * 16))
                    } : el)
            }))
        }
    }

    tagged_videos = (data) => {
        const array = [...this.state.usersInActivity];
        const userIdArray = [];
        if (data.tagged_user_session_ids && data.tagged_user_session_ids.length) {
            data.tagged_user_session_ids.forEach(el => {
                array.forEach(element => {
                    if (el.user_activity_session_id === element.user_activity_session_id) {
                        userIdArray.push(element.user_id);
                    }
                })
            });
        }
        //////////consolele.log("userIdArray", userIdArray);
        if (userIdArray.length) {
            userIdArray.forEach(el => {
                array.forEach(element => {
                    if (el === element.user_id && element.is_best_attempt === true) {
                        element['video_stream'] = true;
                        element['showStream'] = true;
                        //////////consolele.log("matched user", element);
                    }
                })
            });
        }
        this.setState({
            usersInActivity: [...array]
        })
    }


    toggle = () => {
        var video = document.getElementById('video-player');
        if (video !== undefined) {
            video.muted = !video.muted;
            this.setState({ mute: video.muted });
        }
    }

    webPreviewOff = (data) => {
        //////////consolele.log("webPreviewOff", data);
        const obj = {
            user_activity_session_id: data.user_activity_session_id,
            current_duration: this.state.totalSeconds,
            status: this.state.playerPlayStatus ? 'PLAY' : 'PAUSE'
        }
        setTimeout(() => {
            this.state.socket.emit("web_preview_off", obj, (el) => {
                // //////////consolele.log("webPreviewOff emit log", el);
                this.end_activity();
            });
        }, 300);
    }

    callListners = () => {
        ////////////consolele.log("Listner Called");
        this.state.socket.on("activity_ended", this.end_activity);
        this.state.socket.on("web_preview_off", this.webPreviewOff);
        this.state.socket.on("video_tagged", (e) => this.start_stream(e, 1));
        this.state.socket.on("video_untagged", (e) => this.end_stream(e, 1));
        this.state.socket.on("self_heartbeat_sync", this.self_heartbeat);
        this.state.socket.on("sync_tagged_videos", this.tagged_videos);
        this.state.socket.on("ondemand_activity_sync", this.end_activity);
    }

    getData = () => {
        const formData = {
            'gym_id': this.state.workoutData.gym_id,
            'activity_id': this.state.workoutData.activity_id,
            'duration_in_ms': new Date(this.state.workoutData.ended_at).getTime() - new Date(this.state.workoutData.started_at).getTime()
        };
        axios.post(ApiURL._ondemandActivity, formData).then(response => {
            let userBestInfo = {};
            if (response.data.data.usersInActivity && response.data.data.usersInActivity.length) {
                response.data.data.usersInActivity.forEach((element, index) => {
                    element['calories_burnt'] = element['calories_burnt'] ? Math.round(element['calories_burnt']) : 0;
                    element['maxHeartRate'] = Math.round(208 - (0.7 * element['age']));
                    element['rank'] = index + 1;
                    element['heartrate'] = Math.round(element['avg_heart_rate']);
                    element['opacity'] = 'unset';
                    element['color'] = colorSelector(element['avg_heart_rate'] ? Math.round((element['avg_heart_rate'] / element['maxHeartRate']) * 100) : 0);
                    if (element['ant_media_recording'] !== null && element['is_best_attempt'] &&
                        (element.participant_video_visibility_in_attempt === null || element.participant_video_visibility_in_attempt === 'ALL' ||
                            element.participant_video_visibility_in_attempt === 'FRIENDS_ONLY')) {
                        element['video_stream'] = true;
                    } else {
                        element['video_stream'] = false;
                    }
                    if (element['is_best_attempt'] &&
                        element.user_id === response.data.data.new_user.user_id) {
                        userBestInfo = { ...element };
                        element['is_best_attempt'] = false;
                        element['ant_media_recording'] = null;
                        //////////consolele.log("userBestInfo", userBestInfo, element);
                    }

                    if (element.user_activity_session_id ===
                        response.data.data.new_user.user_activity_session_id) {
                        //////////consolele.log("Session Id Matched", element, userBestInfo);
                        element['is_best_attempt'] = true;
                        element['total_attempts_yet'] = userBestInfo.total_attempts_yet ? userBestInfo.total_attempts_yet : 0;
                        element['ant_media_recording'] = userBestInfo.ant_media_recording ? userBestInfo.ant_media_recording : null;
                        if (element['ant_media_recording'] !== null && element['is_best_attempt']) {
                            element['video_stream'] = true;
                        } else {
                            element['video_stream'] = false;
                        }
                    }
                });
            }
            this.setState({
                usersInActivity: [...response.data.data.usersInActivity],
                new_user: { ...response.data.data.new_user },
                totalMembers: response.data.data.usersInActivity.length
            });
            setTimeout(() => {
                this.callListners();
            }, 200);
            setTimeout(() => {
                this.syncSocketEmitHandler({
                    user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                    web_socket_id: this.state.socket.id
                });
                this.setState({
                    firstTaggedEmit: true
                });
            }, 1500);
        }, error => {
            ////consolele.log("API ERROR");
        });
    }

    start_stream = (data, status) => {
        //////////consolele.log("show Stream", data, status);
        if (status) {
            this.setState(prevState => ({
                usersInActivity: prevState.usersInActivity.map(el =>
                    el.user_activity_session_id === data.tagged_user_activity_session_id ? {
                        ...el,
                        showStream: true
                    } : el)
            }));
        } else {
            const tagData = {
                user_id: this.state.new_user.user_id,
                user_activity_session_id: this.state.new_user.user_activity_session_id,
                activity_id: this.state.new_user.activity_id,
                tagged_user_id: data.user_id,
                tagged_user_activity_session_id: data.user_activity_session_id,
            }
            this.state.socket.emit("video_tagged", tagData, (obj) => {
            });
            this.setState(prevState => ({
                usersInActivity: prevState.usersInActivity.map(el =>
                    el.user_activity_session_id === data.user_activity_session_id ? {
                        ...el,
                        showStream: true
                    } : el)
            }));
        }
    };

    end_stream = (data, status) => {
        //////////consolele.log(data, status);
        if (status) {
            this.setState(prevState => ({
                usersInActivity: prevState.usersInActivity.map(el =>
                    el.user_activity_session_id === data.tagged_user_activity_session_id ? {
                        ...el,
                        showStream: false
                    } : el)
            }));
        } else {
            const tagData = {
                user_id: this.state.new_user.user_id,
                user_activity_session_id: this.state.new_user.user_activity_session_id,
                activity_id: this.state.new_user.activity_id,
                tagged_user_id: data.user_id,
                tagged_user_activity_session_id: data.user_activity_session_id
            }
            this.state.socket.emit("video_untagged", tagData, (obj) => {
            });
            this.setState(prevState => ({
                usersInActivity: prevState.usersInActivity.map(el =>
                    el.user_activity_session_id === data.user_activity_session_id ? {
                        ...el,
                        showStream: false
                    } : el)
            }));
        }
    }

    toggleBox = (type) => {
        this.setState({
            toggleDiv: {
                ...this.state.toggleDiv,
                [type]: !this.state.toggleDiv[type]
            }
        })
    }


    videoControl = (type) => {
        if (type === 'play') {
            //////////consolele.log("Video Play", this.state.playerPlayStatus);
            this.state.vid.play();
            playerPlaySubscriber.send(true);
            this.setState({
                playerPlayStatus: true
            });
            const data = {
                user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                status: 'PLAY'
            }
            this.state.socket.emit('ondemand_activity_sync', data, (res) => {
                //////////consolele.log("res", res);
            });
            this.startTimer();
        }
        if (type === 'pause') {
            //////////consolele.log("Video Pause", this.state.playerPlayStatus);
            this.state.vid.pause();
            playerPlaySubscriber.send(false);
            this.setState({
                playerPlayStatus: false
            });

            const data = {
                user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                status: 'PAUSE'
            }
            this.state.socket.emit('ondemand_activity_sync', data, (res) => {
                //////////consolele.log("res", res);
            });
            clearInterval(this.myInterval);
        }
        if (type === 'forward' && ((this.state.totalSeconds + 10) <= this.state.MAX)) {
            this.updateSegmentArray([...this.state.segmentDurations], this.state.totalSeconds + 10);
            this.setState({
                totalSeconds: this.state.totalSeconds + 10,
                values: [this.state.totalSeconds + 10]
            });
            seekVideoSubscriber.send(this.state.totalSeconds + 10);
        }
        if (type === 'backward') {
            if ((this.state.totalSeconds - 10) < 0) {
                this.setState({
                    totalSeconds: 0
                });
                seekVideoSubscriber.send(0);
            } else {
                this.updateSegmentArray([...this.state.segmentDurations], this.state.totalSeconds - 10);
                this.setState({
                    totalSeconds: this.state.totalSeconds - 10,
                    values: [this.state.totalSeconds - 10]
                });
                seekVideoSubscriber.send(this.state.totalSeconds - 10);
            }
        }
    }

    mouseMoved = () => {
        //consolele.log("this.state.totalSeconds", this.state.totalSeconds);
        //consolele.log("this.state.seg_total_duration", this.state.seg_total_duration);
        //consolele.log("this.state.MAX", this.state.MAX);
        this.setState({
            onDemandControls: this.state.MAX ? true : false
        });

        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.setState({
                onDemandControls: false
                // onDemandControls: true
            });
        }, 4000);

    }

    render() {
        // console.log("this.state.cur_exercise_data", this.state.cur_exercise_data);
        //consolele.log("this.state.values", this.state.values);
        console.log("this.state.segmentDurations", this.state.segmentDurations);
        // //consolele.log("this.state.MAX", this.state.MAX);
        //////////consolele.log("this.state.segmentDurations", this.state.segmentDurations, this.state.MAX, this.state.seg_total_duration, this.state.seg_total_width);
        const rangeSliders = this.state.segmentDurations.map((el, index) =>
            <div key={index + el.segment_name} style={{ width: 'calc(' + el.width + ' - 5px)', marginLeft: '3px' }}>
                <Range
                    disabled={true}
                    values={el.values}
                    step={el.STEP}
                    min={el.MIN}
                    max={el.MAX}
                    onChange={(values) => {
                        //////////consolele.log("values", values);
                        // this.setState({ values });
                        // this.setState({
                        //     totalSeconds: values[0]
                        // });
                        // seekVideoSubscriber.send(values[0]);
                    }}
                    renderTrack={({ props, children }) => (
                        <div
                            onMouseDown={props.onMouseDown}
                            onTouchStart={props.onTouchStart}
                            style={{
                                ...props.style,
                                height: "12px",
                                display: "flex",
                                width: "100%"
                            }}
                        >
                            <div
                                ref={props.ref}
                                style={{
                                    height: "8px",
                                    width: "100%",
                                    borderRadius: "8px",
                                    background: getTrackBackground({
                                        values: el.values,
                                        colors: ["#FF0042", "#868686"],
                                        min: el.MIN,
                                        max: el.MAX
                                    }),
                                    alignSelf: "center"
                                }}
                            >
                                {children}
                            </div>
                        </div>
                    )}
                    renderThumb={({ props, isDragged }) => (
                        <div
                            {...props}
                            style={{
                                ...props.style,
                                outline: 'none',
                                borderRadius: "25px",
                                backgroundColor: "#F7F9F9",
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                boxShadow: "0px 2px 6px #AAA"
                            }}
                        >
                            <div
                                style={{
                                    backgroundColor: isDragged ? "darkgrey" : "darkgrey"
                                }}
                            />
                        </div>
                    )}
                />
            </div>
        )
        let usersInActivity = [];
        usersInActivity = this.state.usersInActivity.sort((a, b) =>
            b.calories_burnt - a.calories_burnt).map((el, index) => {
                el.rank = index + 1;
                return el
            }
            ).filter(el => el.is_best_attempt === true);
        usersInActivity = usersInActivity.map((el, index) =>
            <MemberList key={index} ondemand={true} type="black" {...el} userId={this.state.new_user.user_id}
                rank={el.rank} startStream={() => this.start_stream(el, 0)}
                endStream={() => this.end_stream(el, 0)} />)
        const videos = Object.values(this.state.usersInActivity).filter(el =>
            el.video_stream && el.showStream && el.is_best_attempt === true).map((el, index) =>
                <UserVideo key={index} ondemand={true} userID={this.state.new_user.user_id} {...el} />);
        // const remainingTime = this.state.cur_segment_data.segment_duration_in_seconds - (this.state.totalSeconds - (this.state.cur_segment_data.combinedMax - this.state.cur_segment_data.segment_duration_in_seconds));
        const remainingTime = this.state.cur_exercise_data !== undefined && Object.keys(this.state.cur_exercise_data).length
            ?
            this.state.cur_exercise_data.exercise_duration_in_seconds - (this.state.totalSeconds - (this.state.cur_exercise_data.combinedExerciseMax - this.state.cur_exercise_data.exercise_duration_in_seconds))
            : 0;

        // //////consolele.log("this.state.totalSEconds", this.state.totalSeconds);
        return (
            <section onMouseMove={this.mouseMoved} className="Casting">
                {this.state.onDemandControls ?
                    <div className="videoControls">
                        {/* <p className="backwardSec">10</p> */}
                        <img src={selectIcon('seekBack')} onClick={() => this.videoControl('backward')} />
                        <img src={selectIcon(this.state.playerPlayStatus ? 'videoPause' : 'videoPlay')} onClick={() => this.videoControl(this.state.playerPlayStatus ? 'pause' : 'play')} />
                        <img src={selectIcon('seekForward')} onClick={() => this.videoControl('forward')} />
                        {/* <p className="forwardSec">10</p> */}
                    </div> : null}

                {
                    <OnDemandPlayer totalSeconds={this.state.totalSeconds} ondemand={true}
                        URL={this.state.workoutData.playback_url !== null ? this.state.workoutData.playback_url :
                            (this.state.workoutData.ondemand_videos[this.state.workoutData.ondemand_videos.length - 1] + '?v=1.0')}
                        ivsStatus={true} />
                }

                <div className="infoBox">
                    <div className="infoImageBox">
                        <img className="infoImage" alt="placeholder" src={this.state.workoutData.logo_thumb} />
                        {/* <button type="button" onClick={() => this.videoControl('play')}>Play</button> */}
                    </div>

                    {this.state.showSeekBar ?
                        <div className="rangeSlider">
                            <p className="segmentTime">  {this.convertTime(this.state.totalSeconds)}</p>
                            <p className="activityTime"> {this.convertTime(this.state.MAX)}</p>
                            {remainingTime >= 0 &&
                                this.state.cur_exercise_data !== undefined ?
                                <p className="segmentName">{this.state.cur_exercise_data.exercise_name}</p> : null}
                            {/* {this.state.onDemandControls ? */}
                            <div className="hiddenSeekbar" style={{
                                width: 'calc(' + this.state.seg_total_width + '%' + ' - ' + (this.state.segmentDurations.length * 5) + 'px)',
                                opacity: this.state.is_WorkoutBuilder_Available ? '0' : '1'
                            }}>
                                <Range
                                    values={this.state.values[0] <= this.state.seg_total_duration ? this.state.values : [this.state.seg_total_duration]}
                                    step={this.state.STEP}
                                    min={this.state.MIN}
                                    max={this.state.seg_total_duration <= this.state.MAX ? this.state.seg_total_duration : this.state.MAX}
                                    onChange={(values) => {
                                        this.updateSegmentArray([...this.state.segmentDurations], values[0])
                                        this.setState({ values });
                                        this.setState({
                                            totalSeconds: values[0]
                                        });
                                        seekVideoSubscriber.send(values[0]);
                                    }}
                                    renderTrack={({ props, children }) => (
                                        <div
                                            onMouseDown={props.onMouseDown}
                                            onTouchStart={props.onTouchStart}
                                            style={{
                                                ...props.style,
                                                height: "12px",
                                                display: "flex",
                                                width: "100%"
                                            }}
                                        >
                                            <div
                                                ref={props.ref}
                                                style={{
                                                    height: "8px",
                                                    width: "100%",
                                                    borderRadius: "4px",
                                                    background: getTrackBackground({
                                                        values: this.state.values,
                                                        colors: ["#F22B5D", "#868686"],
                                                        min: this.state.MIN,
                                                        max: this.state.MAX
                                                    }),
                                                    alignSelf: "center"
                                                }}
                                            >
                                                {children}
                                            </div>
                                        </div>
                                    )}
                                    renderThumb={({ props, isDragged }) => (
                                        <div
                                            {...props}
                                            style={{
                                                ...props.style,
                                                outline: 'none',
                                                borderRadius: "25px",
                                                backgroundColor: "#F7F9F9",
                                                display: "flex",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                boxShadow: "0px 2px 6px #AAA"
                                            }}
                                        >
                                            <div
                                                style={{
                                                    height: "1px",
                                                    width: "1px",
                                                    backgroundColor: isDragged ? "darkgrey" : "darkgrey"
                                                }}
                                            />
                                        </div>
                                    )}
                                />
                            </div>
                            {/* : null} */}
                            {rangeSliders}
                        </div> : null}

                    <div className="time">
                        {this.state.showSeekBar && remainingTime >= 0 && this.state.segmentDurations.length &&
                            this.state.cur_exercise_data !== undefined ?
                            <CircularProgressbarWithChildren value={remainingTime} maxValue={this.state.cur_exercise_data.exercise_duration_in_seconds} strokeWidth={3}
                                styles={{ path: { stroke: '#FF0042' }, trail: { stroke: 'rgba(0,0,0,0.001)' } }}>
                                <div className={this.convertTime(remainingTime).length > 6 ? "newInnerCircle" : "innerCircle"}>
                                    {Object.keys(this.state.cur_segment_data).length ?
                                        <p style={{ textAlign: 'center' }}>
                                            {remainingTime > 59 ? this.convertTime(remainingTime) : remainingTime > 9 ? remainingTime : '0' + remainingTime}
                                        </p>
                                        : null}
                                </div>
                            </CircularProgressbarWithChildren> : null
                        }
                    </div>
                </div>

                <div className="membersBox"
                    style={{ justifyContent: videos.length ? 'space-between' : 'flex-end' }}>
                    {videos.length ?
                        <div className="videoContainer" style={{ position: 'relative' }}>
                            {/* <button className={this.state.toggleDiv.video ? "hideVideoNew" : "hideVideo"} onClick={() => this.toggleBox('video')}>{!this.state.toggleDiv.video ? 'Hide' : 'Show'}</button> */}
                            <button style={{ flexDirection: !this.state.toggleDiv.video ? 'row-reverse' : 'row' }}
                                className={this.state.toggleDiv.video ? 'hideVideoNew' : 'hideVideo'} onClick={() => this.toggleBox('video')}>
                                {/* {!this.state.toggleDiv.video ? 'Hide' : 'Friends'} */}
                                <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={
                                    !this.state.toggleDiv.video ? faChevronLeft : faChevronRight} />
                            </button>

                            <div className="videoSubContainer" style={{ opacity: this.state.toggleDiv.video ? 0 : 1 }}>
                                <div id="xyz-board" className="friendsBox">
                                    {/* <div className="friendsTitle">
                                                <p>Friends</p>
                                            </div> */}
                                    <div className="videoList">
                                        {videos}
                                    </div>
                                </div>
                            </div>



                        </div>

                        : null}
                    <div className="boardContainer" style={{ position: 'relative' }}>
                        {usersInActivity.length ?
                            <button style={{ flexDirection: !this.state.toggleDiv.list ? 'row' : 'row-reverse' }}
                                className={this.state.toggleDiv.list ? 'hideUnhidenew' : 'hideUnhide'} onClick={() => this.toggleBox('list')}>
                                {/* {!this.state.toggleDiv.list ? 'Hide' : 'Board'} */}
                                <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={
                                    !this.state.toggleDiv.list ? faChevronRight : faChevronLeft} />
                            </button>
                            : null}

                        <div className="boardSubContainer" style={{
                            height: usersInActivity.length > 6 ? '100%' : '400px',
                            opacity: this.state.toggleDiv.list ? 0 : 1
                        }} >
                            <div className="toggleContainer" style={{ width: 'auto' }}>
                                <div className="membersCount">
                                    <img src={selectIcon('usersList')} />
                                    <p>{usersInActivity.length}</p>
                                </div>
                            </div>
                            <div className="memberListContainer">
                                {usersInActivity}
                            </div>
                        </div>

                    </div>
                </div>

                {/* </div> */}
                <div className="statsContainer">
                    {this.state.toggleDiv.stats ?
                        <button style={{ flexDirection: 'row' }}
                            className='showStats' onClick={() => this.toggleBox('stats')}>
                            <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={faChevronUp} />
                        </button> : null
                    }
                    <div className="statsBox" style={{ opacity: this.state.toggleDiv.stats ? 0 : 1, transition: 'opacity 0.3s' }}>
                        <img className="statsImage" src={selectIcon('backgroundStats')} />
                        <button style={{ flexDirection: 'row', display: this.state.toggleDiv.stats ? 'none' : 'block' }}
                            className='hideStats' onClick={() => this.toggleBox('stats')}>
                            <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={faChevronDown} />
                        </button>
                        <UserStats {...this.state.selfData}
                            rank={this.state.usersInActivity.findIndex(el => el.user_activity_session_id === this.state.new_user.user_activity_session_id) + 1} />
                    </div>
                    <div className="fullScreen">
                        <img className="fullScreenImage" src={selectIcon(this.state.fullScreen ? 'exitFullscreen' : 'fullScreen')} onClick={this.state.fullScreen ? this.closeFullscreen : this.openFullscreen} />
                        <img className="audioIcon" onClick={this.toggle}
                            src={selectIcon(!this.state.mute ? 'volumeUp' : 'volumeMute')} />
                    </div>
                </div>
            </section>
        )
    }
}

export default Ondemand;