import React, { Component } from 'react';
import IO from 'socket.io-client/dist/socket.io.js';
import QRCode from "react-qr-code";
import './Home.scss';
import Logo from '../../assets/Images/logo.png';
import loginGuide from '../../assets/videos/homePage.mp4';
import videoThumbnail from '../../assets/videos/videoThumbnail1.png';
import { env } from '../../services/CommonFunction';
import { selectIcon } from '../../components/UI/selector';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faRedo } from '@fortawesome/free-solid-svg-icons';


class Home extends Component {

    state = {
        QRCodeID: '',
        video: null,
        isVideoPlaying: false,
        isVideoEnded: false,
        socket: IO(env('OnDemandSocket'), { transports: ['websocket'] }),
        // socket: IO(env('OnDemandSocket') + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uX2lkIjoiYzk1MzQ1MDUtODNjMi00MTIzLWJlNjQtY2RhMWQ2MjlkNTNkIiwiZGF0ZSI6IjIwMjEtMDUtMjJUMDU6MDk6MzUuOTYzWiIsImlhdCI6MTYyMTY2MDE3NX0.8sfzRwmOcFtnC-LPwAEtB4I7_paWALA3k6ALmRQFjAQ', { transports: ['websocket'] }),
        workoutData: {},
        screenWidth: 250
    }

    componentDidMount() {
        this.state.socket.on('connect', this.socketConnected)
        this.state.socket.on("sync_workout_request", this.syncWorkout);
        if (localStorage.getItem('workoutData') || localStorage.getItem('accessToken')
            || localStorage.removeItem('ondemandData') || localStorage.removeItem('startTime')) {
            localStorage.removeItem('workoutData');
            localStorage.removeItem('accessToken');
            localStorage.removeItem('startTime');
            localStorage.removeItem('ondemandData');
        }
        if (localStorage.getItem('videoPausedOnce')) {
            localStorage.removeItem('videoPausedOnce')
        }
        this.setState({
            screenWidth: this.setQRcodeWidth(window.innerWidth)
        })
        window.addEventListener('orientationchange', function () {
            var originalBodyStyle = getComputedStyle(document.body).getPropertyValue('orientation');
            console.log(originalBodyStyle);
            // document.body.style.display = 'none';
            // setTimeout(function () {
            //     document.body.style.display = originalBodyStyle;
            // }, 10);
        });
        window.addEventListener('resize', this.handleResize)

    }

    componentWillUnmount() {
        console.log("Component unmounted");
        this.state.socket.disconnect();
    }

    handleResize = () => {
        // console.log("window.innerHeight", window.innerHeight, "window.innerWidth", window.innerWidth, "window.orientation", window.orientation);
        // if (window.innerWidth > 319 && window.innerWidth < 767) {

        // }
        this.setState({
            screenWidth: this.setQRcodeWidth(window.innerWidth)
        })
    }


    setQRcodeWidth = (width) => {
        // console.log(width);
        switch (true) {
            case (width >= 1224 && width < 1501):
                return 230;
            case (width >= 1024 && width < 1224):
                return 210;
            case (width >= 768 && width < 1024):
                return 170;
            case (width >= 480 && width < 768):
                return 250;
            case (width >= 320 && width < 480):
                return 230;
            default:
                return 250;
        }
    }

    syncWorkout = (data, access_token) => {
        console.log("sync_workout_request", data, access_token);
        localStorage.setItem('workoutData', JSON.stringify(data));
        localStorage.setItem('ondemandData', JSON.stringify(access_token));
        localStorage.setItem('accessToken', access_token.access_token);
        if (Object.keys(data).length) {
            if (data.is_ondemand_workout) {
                this.props.history.push('/Ondemand');
            } else {
                this.props.history.push('/Casting');
            }
        }
    }

    socketConnected = () => {
        console.log("connected", this.state.socket.id);
        this.setState({
            QRCodeID: this.state.socket.id
        });
    }

    playVideo = () => {
        if (!this.state.isVideoPlaying) {
            this.setState({
                isVideoPlaying: true
            });
            // this.setState({ video: document.getElementById('login-video') });
            // setTimeout(() => {
            //     this.state.video.play();
            // }, 1000);
        }
    }

    videoEnded = () => {
        this.setState({
            isVideoPlaying: false,
            isVideoEnded: true
        });
    }

    render() {
        const videoLink = "https://tribe-website-videos.s3.amazonaws.com/homePage.mp4";
        return (
            <section className="Home">
                <div className="pinkDiv">
                    <img className="logoImage" src={Logo} alt="LOGO" />
                </div>
                <div className="containerDiv">
                    <div className="containerBox">
                        <div className="scanContainer">
                            <div className="stepsDiv">
                                <h2>To cast your workout on this device</h2>
                                <p>1. Open Tribe app on your phone</p>
                                <p>2. Join Activity/Workout</p>
                                <p className="imagesIconsClass">3. Tap
                                        <img className="scanIcon" src={selectIcon('scanWifi')} />
                                    on tribe application</p>
                                <p>4. Use your phone to scan this QR code</p>
                            </div>
                            <div className="scan">
                                <QRCode value={this.state.QRCodeID + '_tribeWeb'} size={this.state.screenWidth} />
                            </div>
                        </div>
                        <div className="videoContainer">
                            <div className="videoTagDiv" onClick={this.playVideo}
                                style={{ cursor: !this.state.isVideoPlaying ? 'pointer' : 'unset' }}>
                                {!this.state.isVideoPlaying ?
                                    <React.Fragment>
                                        <div className="playIcon">
                                            <FontAwesomeIcon style={{ color: '#FFFFFF' }}
                                                icon={this.state.isVideoEnded ? faRedo : faPlay} />
                                        </div>
                                        <img className="videoImage" src={videoThumbnail} />
                                    </React.Fragment> :
                                    <video id='login-video' onEnded={this.videoEnded} className="videoImage" autoPlay controls src={videoLink} type="video/mp4" />}
                            </div>
                            <p>Need help to get started?</p>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <p> Copyright (C) 2022 Tribe Fitness Inc   </p>
                </div>
            </section>

        );
    }
}

export default Home;