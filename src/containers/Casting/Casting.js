import React, { Component } from 'react'
import IO from 'socket.io-client/dist/socket.io.js';
import ApiURL from '../../services/ApiURL';
import axios from '../../axios';
import { env, randomString } from '../../services/CommonFunction';
import moment from "moment";
import './Casting.scss';
import { colorSelector, selectIcon } from '../../components/UI/selector';
import BoardUser from './BoardUser/BoardUser';
import UserVideo from './UserVideo/UserVideo';
import UserStats from './UserStats/UserStats';
import PlayStream from './MemberStream/MemberStream';
import AmazonIVSWorkaround from '../../components/Video/AmazonIVSWorkaround';
import { ivsSubscriber, imageError } from '../../services/subscriberService';
import { Spring } from 'react-spring/renderprops';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight, faVolumeUp, faVolumeMute, faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import MemberList from './memberList/memberList';
import RangeSlider from './RangeSlider/RangeSlider';
import { Range, getTrackBackground } from "react-range";
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';

class Casting extends Component {

    state = {
        workoutData: {},
        socket: IO(env('OnDemandSocket') + localStorage.getItem('accessToken'), { transports: ['websocket'] }),
        allUsers: {},
        userActivityData: {},
        coachData: {},
        gymName: '',
        isInternetConnected: null,
        showChatDropdown: false,
        values: [1],
        STEP: 1,
        MIN: 0,
        MAX: 0,
        userType: 'everyone',
        hasMore: true,
        disconnectTime: 0,
        chatUxToggle: false,
        firstTaggedEmit: false,
        streaming: false,
        ivsStatus: false,
        stream_key_arn: null,
        playBackURL: '',
        activity_id: '',
        started_at: '',
        totalSeconds: 0,
        fullScreen: false,
        mute: false,
        toggleDiv: {
            list: false,
            stats: false,
            video: false,
        },
        totalMembers: 0,
        selfData: {},
        userId: '',
        userSessionId: '',
        segmentDurations: [],
        showSeekBar: false,
        cur_segment_data: {},
        cur_exercise_data: {},
        total_activity_duration_in_seconds: 0,
        segment_duration_max: null,
        segment_duration_prev_max: null,
        messageList: [],
        newMessageCount: 0,
        messageText: '',
        staffsInActivity: {},
    };

    componentDidMount() {
        console.log();
        const ondemandData = { ...JSON.parse(localStorage.getItem('ondemandData')) };
        const currentTime = new Date().getTime() / 1000;
        let startTime = 0;
        if (localStorage.getItem('startTime')) {
            startTime = parseInt(localStorage.getItem('startTime'));
        } else {
            startTime = (new Date().getTime() - (ondemandData.time_elapsed * 1000)) / 1000;
            localStorage.setItem('startTime', startTime);
        }
        this.state.socket.on('connect', this.socketConnected);
        this.state.socket.on('disconnect', this.socketDisconnected);
        this.setState({
            workoutData: { ...JSON.parse(localStorage.getItem('workoutData')) },
            ondemandData: { ...ondemandData },
            totalSeconds: ondemandData.time_elapsed ? (Math.round(currentTime) - Math.round(startTime)) + 1 : 0,
            newMessageCount: localStorage.getItem('messageCount') ? localStorage.getItem('messageCount') : 0
        });
        setTimeout(() => {
            this.joinActivity();
        }, 1000);

        // setTimeout(() => {
        //     this.startTimer();
        // }, 200);
    }


    callListners = () => {
        this.state.socket.on("heartbeat", this.updateHeartRate);
        this.state.socket.on("rankings", this.updateRanking);
        this.state.socket.on("new_user", this.addNewUser);
        this.state.socket.on("activity_ended", this.endActivity);
        this.state.socket.on("user_left", this.removeUser);
        this.state.socket.on("web_preview_off", this.endActivity);
        this.state.socket.on("video_tagged", (e) => this.startStream(e, 1));
        this.state.socket.on("video_untagged", (e) => this.endStream(e, 1));
        this.state.socket.on("activity_ended", this.endActivity);
        this.state.socket.on("user_removed", this.endActivity);
        this.state.socket.on("self_heartbeat_sync", this.selfHeartbeat);
        this.state.socket.on("live_stream_ended", this.setStream);
        this.state.socket.on("live_stream_started", this.setStream);
        this.state.socket.on("publish_stream_started", this.playMemberStream);
        this.state.socket.on("publish_stream_stopped", this.stopMemberStream);
        this.state.socket.on("message", this.messageReceived);
        this.state.socket.on("staff_joined", this.staffJoined);
        this.state.socket.on("staff_left", this.staffLeft);

        setTimeout(() => {
            let array = [];
            const obj = {
                'user_id': this.state.userId,
                'activity_id': this.state.userActivityData.activity_id
                // 'offset': 0
            }
            this.state.socket.emit("get_chat", obj, (data) => {
                console.log(data);
                if (data.length) {
                    array = data.map(el => JSON.parse(el));
                    console.log("getChat", array);
                    this.setState({
                        messageList: [...array],
                        hasMore: array.length < 20 ? false : true
                    });
                } else {
                    this.setState({
                        hasMore: false
                    });
                }
            });
        }, 500);
    }
    staffLeft = (data) => {
        const { infoOfLeftStaff } = data;
        const { staffsInActivity } = this.state;
        if(this.CheckValue(infoOfLeftStaff && staffsInActivity[infoOfLeftStaff.user_id])) {
            const { socket_id } = staffsInActivity[infoOfLeftStaff.user_id];
            const index = socket_id.findIndex(el => el === infoOfLeftStaff.socket_id);
            if(index > -1) {
                socket_id.splice(index,1)    
            }
            if(!socket_id.length) {
                delete staffsInActivity[infoOfLeftStaff.user_id]; 
                this.setState({
                    staffsInActivity: {...staffsInActivity}
                });
            }
        }
    }

    CheckValue = (value) => value !== null && value !== undefined && value !== "null" && value !== "undefined";

    // =================  staffJoined ========================
    staffJoined = (data) => {
        const { infoOfJoinedStaff } = data;
        const { staffsInActivity } = this.state;
        console.log(data);
        let role = typeof infoOfJoinedStaff.roles === 'string'?[infoOfJoinedStaff.roles]:infoOfJoinedStaff.roles;
        if (this.checkRole('gym',role)) {
            return;
        }

        if(this.CheckValue(staffsInActivity[infoOfJoinedStaff.user_id])) {
            const { socket_id } = staffsInActivity[infoOfJoinedStaff.user_id];
            socket_id.push(data.socket_id);
            staffsInActivity[infoOfJoinedStaff.user_id].socket_id = this.StaffSort(socket_id);
        } else {
            this.setState(prevState => ({
                staffsInActivity: { ...prevState.staffsInActivity, [infoOfJoinedStaff.user_id]: this.setStaff({...infoOfJoinedStaff,socket_id:data.socket_id},role)}
            }))
        }
    }

    setStaff = (el, role) => ({
        first_name: el.first_name,
        last_name: el.last_name,
        profile_pic_original: el.profile_pic_original,
        profile_pic_thumb: el.profile_pic_thumb,
        // roles: checkRole('gym', role) ? 'owner' : 'coach',
        socket_id: [el.socket_id],
        user_id: el.user_id,
    });

    StaffSort = (data) => (
        data.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) === index;
        })
    )

    componentWillUnmount() {
        ////console.log("Component unmounted");

        const obj = {
            'user_id': this.state.userId,
            'activity_id': this.state.userActivityData.activity_id,
            'user_activity_session_id': this.state.userActivityData.user_activity_session_id
        }
        this.state.socket.emit("web_preview_off", obj, (data) => {
            ////console.log("Web Off");
        });
        this.state.socket.disconnect();
        clearInterval(this.myInterval)
    }

    checkLength = (data) => (
        data < 10 ? '0' + data : data
    )

    convertTime = (totalSeconds, type = ':') => {
        const second = totalSeconds % 60;
        const minute = parseInt((totalSeconds) / 60, 10);
        const minutes = parseInt((minute) % 60, 10);
        const hour = parseInt((minute) / 60, 10);

        let time = '';
        if (hour > 0) {
            time += this.checkLength(hour) + type;
        }
        time += this.checkLength(minutes) + type;
        time += this.checkLength(second);
        return time;
    }

    setTime = () => {
        let array = [...this.state.segmentDurations];
        array.forEach(el => {
            if (this.state.totalSeconds <= el.combinedMax && ((el.combinedMax - el.segment_duration_in_seconds) <= this.state.totalSeconds)) {
                // console.log("el", el);
                el['values'] = [(this.state.totalSeconds - (el.combinedMax - el.segment_duration_in_seconds)) + 1];
                this.setState({
                    cur_segment_data: { ...el },
                    cur_exercise_data: el.exercises
                        .find(ed => this.state.totalSeconds < ed.combinedExerciseMax)
                });
            }
        });
        this.setState(prevState => ({
            totalSeconds: prevState.totalSeconds + 1,
            segmentDurations: [...array]
        }));
    }

    activityPolling = () => {
        const data = {
            user_activity_session_id: this.state.ondemandData.user_activity_session_id,
            status: 'PROGRESS'
        }
        // //console.log("Polling", data);
        this.state.socket.emit('ondemand_activity_sync', data, (res) => {
            // //console.log("Polling");
        });
    }

    startTimer = () => {
        this.myInterval = setInterval(() => {
            if (!this.state.isInternetConnected) {
                //console.log("disconnected", this.state.totalSeconds, this.state.disconnectTime, (this.state.totalSeconds - this.state.disconnectTime));
                // if ((this.state.totalSeconds - this.state.disconnectTime) > 9) {
                //     //console.log(this.state.totalSeconds - this.state.disconnectTime);
                //     this.endActivity();
                // }
            } else if (!(this.state.totalSeconds % 10)) {
                this.activityPolling();
            }
            var video = document.getElementById("video-player");
            if (video !== undefined && this.state.mute !== video.muted) {
                this.setState({ mute: video.muted });
            }
            this.setTime()
        }, 1000)
    }

    toggle = () => {
        var video = document.getElementById('video-player');
        if (video !== undefined) {
            video.muted = !video.muted;
            this.setState({ mute: video.muted });
        }
    }

    socketConnected = () => {
        ////console.log("socket connected");
        const ondemandData = { ...JSON.parse(localStorage.getItem('ondemandData')) };
        ////console.log(ondemandData);
        this.socketEmitHandler({
            activity_id: ondemandData.activity_id,
            user_activity_session_id: ondemandData.user_activity_session_id
        });
    }

    socketDisconnected = (data) => {
        ////console.log("Disconnected", data);
        ////console.log('Socket-connected-check', this.state.socket.connected);
        //console.log("Disconnected", data);
        this.setState({
            isInternetConnected: false,
            disconnectTime: this.state.totalSeconds
        });
    }

    socketEmitHandler = (data) => {
        console.log("web_workout_sync_request", data);
        data['isCoach'] = true;
        this.state.socket.emit("web_workout_sync_request", data, (obj) => {
            //console.log("web_workout_sync_request", obj);
            if (obj.user_status_in_activity === 'ENDED' ||
                obj.web_preview === 'false') {
                this.endActivity();
            } else {
                this.setState({
                    isInternetConnected: true,
                    disconnectTime: 0
                });
                if (this.state.firstTaggedEmit) {
                    this.syncSocketEmitHandler({
                        user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                        web_socket_id: this.state.socket.id
                    });
                }
            }

        });
    };

    syncSocketEmitHandler = (data) => {
        ////console.log("sync_tagged_videos", data);
        this.state.socket.emit("sync_tagged_videos", data, (obj) => {
            // //console.log("sync_tagged_videos", data);
        });
    }

    updateHeartRate = (data) => {
        // ////console.log(data);
        // ////console.log("updateHeartRate", data);     
        const obj = { ...this.state.allUsers };
        // ////console.log(obj);
        obj[data.user_id]['color'] = colorSelector(data.max_heart_rate_percent ? Math.round(data.max_heart_rate_percent) : 0);
        obj[data.user_id]['calories_burnt'] = data.calories_burnt ? Math.round(data.calories_burnt) : 0;
        obj[data.user_id]['heartrate'] = data.heartrate ? Math.round(data.heartrate) : 0;
        this.setState({
            allUsers: { ...obj }
        });
    }

    updateRanking = (data) => {
        const obj = {};
        data.forEach(el => {
            obj[el.user_id] = this.state.allUsers[el.user_id];
        });
        // ////console.log("Updated Ranking", obj);
        this.setState({
            allUsers: { ...obj }
        });
    }

    addNewUser = (data) => {
        ////console.log("addNewUser", data);
        const obj = { ...this.state.allUsers };
        if (this.state.allUsers.hasOwnProperty(data.user_id)) {
            ////console.log("Already Existed");
            obj[data.user_id]['color'] = '#808080';
            obj[data.user_id]['opacity'] = 'unset';
            obj[data.user_id]['heartrate'] = 0;
        } else {
            ////console.log("Newly added");
            obj[data.user_id] = {
                ...data,
                color: '#808080',
                maxHeartRate: Math.round(208 - (0.7 * data['age'])),
                calories_burnt: 0,
                opacity: 'unset',
                heartrate: 0
            };
        }
        this.setState({
            allUsers: { ...obj }
        });
    }

    removeUser = (data) => {
        if (this.state.userActivityData.user_id === data.user_id) {
            this.state.socket.disconnect();
            clearInterval(this.myInterval)
            this.endActivity();
        }
        const obj = { ...this.state.allUsers };
        obj[data.user_id]['color'] = '#808080';
        obj[data.user_id]['opacity'] = '0.6';
        obj[data.user_id]['max_heart_rate_percent'] = 0;
        obj[data.user_id]['heartrate'] = 0;
        obj[data.user_id]['user_status_in_activity'] = 'ENDED';
        this.setState({
            allUsers: { ...obj }
        });
    }

    endActivity = (data) => {
        //console.log("End Activity");
        localStorage.removeItem('workoutData');
        localStorage.removeItem('accessToken');
        ivsSubscriber.sendIvsData({
            ivsStatus: false,
            playbackUrl: ""
        });
        this.setState({
            ivsStatus: false,
            playBackURL: ""
        });
        setTimeout(() => {
            this.props.history.push('/Home');
        }, 200);
    }

    setStream = (data) => {
        ////console.log("setStream", data);
        if (data === undefined) {
            setTimeout(() => {
                ivsSubscriber.sendIvsData({
                    ivsStatus: false,
                    playbackUrl: ""
                });
            }, 200);
            this.setState({
                ivsStatus: false,
                playBackURL: ""
            });
        } else {
            window.location.reload();
            setTimeout(() => {
                ivsSubscriber.sendIvsData({
                    ivsStatus: true,
                    playbackUrl: data.playbackUrl
                });
            }, 200);
            this.setState({
                ivsStatus: true,
                playBackURL: data.playbackUrl
            });
        }
    }

    playMemberStream = (data) => {
        // //console.log(data);
        if ((this.state.allUsers.hasOwnProperty(data.user_id) && this.state.userId === data.user_id) || (this.state.allUsers.hasOwnProperty(data.user_id) && (data.participant_video_visibility_in_attempt === null || data.participant_video_visibility_in_attempt === 'ALL' ||
            data.participant_video_visibility_in_attempt === 'FRIENDS_ONLY'))) {
            this.setState(prevState => ({
                allUsers: {
                    ...prevState.allUsers,
                    [data.user_id]: { ...prevState.allUsers[data.user_id], video_stream: true }
                }
            }));
        }
    }

    stopMemberStream = (data) => {
        // //console.log(data);
        const obj = { ...this.state.allUsers };
        if (obj.hasOwnProperty(data.user_id)) {
            if (obj[data.user_id].stream) {
                ////console.log("obj[data.user_id]", obj[data.user_id]);
                obj[data.user_id].stream.stop();
                delete obj[data.user_id].stream;
            }
            obj[data.user_id]['video_stream'] = false;
            obj[data.user_id]['showStream'] = false;
        }
        this.setState({
            allUsers: { ...obj }
        });
    }

    startStream = (data, status) => {
        // //console.log("show Stream", data, status);
        if (status) {
            this.setState(prevState => ({
                allUsers: {
                    ...prevState.allUsers,
                    [data.tagged_user_id]: { ...prevState.allUsers[data.tagged_user_id], showStream: true }
                }
            }));
            setTimeout(() => {
                this.setState(prevState => ({
                    allUsers: {
                        ...prevState.allUsers,
                        [data.tagged_user_id]: {
                            ...prevState.allUsers[data.tagged_user_id]
                            , stream: PlayStream({ ...this.state.allUsers[data.tagged_user_id], user_activity_session_id: this.state.allUsers[data.tagged_user_id]['user_activity_session_id'] })
                        }
                    }
                }));
            }, 300);
        } else {
            const tagData = {
                user_id: this.state.userId,
                user_activity_session_id: this.state.userSessionId,
                activity_id: data.activity_id,
                tagged_user_id: data.user_id,
                tagged_user_activity_session_id: data.user_activity_session_id,
            }
            ////console.log(tagData);
            this.state.socket.emit("video_tagged", tagData, (obj) => {
                ////console.log(obj);
            });
            this.setState(prevState => ({
                allUsers: {
                    ...prevState.allUsers,
                    [data.user_id]: { ...prevState.allUsers[data.user_id], showStream: true }
                }
            }));
            setTimeout(() => {
                this.setState(prevState => ({
                    allUsers: {
                        ...prevState.allUsers,
                        [data.user_id]: {
                            ...prevState.allUsers[data.user_id]
                            , stream: PlayStream({ ...this.state.allUsers[data.user_id], user_activity_session_id: this.state.allUsers[data.user_id]['user_activity_session_id'] })
                        }
                    }
                }));
            }, 300);
        }

    };

    endStream = (data, status) => {
        // //console.log("EndStream", data);
        if (status) {
            this.setState(prevState => ({
                allUsers: {
                    ...prevState.allUsers,
                    [data.tagged_user_id]: { ...prevState.allUsers[data.tagged_user_id], showStream: false, stream: null }
                }
            }));
        } else {
            const tagData = {
                user_id: this.state.userId,
                user_activity_session_id: this.state.userSessionId,
                activity_id: data.activity_id,
                tagged_user_id: data.user_id,
                tagged_user_activity_session_id: data.user_activity_session_id,
            }
            ////console.log(tagData);
            this.state.socket.emit("video_untagged", tagData, (obj) => {
                ////console.log(obj);
            });
            this.setState(prevState => ({
                allUsers: {
                    ...prevState.allUsers,
                    [data.user_id]: { ...prevState.allUsers[data.user_id], showStream: false, stream: null }
                }
            }));
        }
    };

    selfHeartbeat = (data) => {
        // ////console.log(data);
        this.setState({
            selfData: { ...data }
        });
    }

    tagged_videos = (data) => {
        //console.log("tagged_videos", data);
        ////console.log("this.state.allUsers", this.state.allUsers);
        const array = [...Object.values(this.state.allUsers)];
        const userIdArray = [];
        if (data.tagged_user_session_ids && data.tagged_user_session_ids.length) {
            data.tagged_user_session_ids.forEach(el => {
                array.forEach(element => {
                    if (el.user_activity_session_id === element.user_activity_session_id) {
                        userIdArray.push({ tagged_user_id: element.user_id });
                    }
                })
            });
        }
        //console.log("userIdArray", userIdArray);
        if (userIdArray.length) {
            userIdArray.forEach(el => {
                this.startStream(el, 1);
            });
        }
    }

    chatToggle = () => {
        if (!this.state.chatUxToggle) {
            localStorage.removeItem("messageCount");
        }
        this.setState({
            chatUxToggle: !this.state.chatUxToggle,
            newMessageCount: 0
        });
    }

    checkRole = (status, roles) => {
        if (roles !== undefined) {
            if (status === 'gym') {
                return roles.includes('GYM_OWNER') || roles.includes('GYM_ADMIN');
            } else if (status === 'coach') {
                return roles.includes('COACH');
            } else if (status === 'all') {
                return roles.includes('COACH') && (roles.includes('GYM_OWNER') || roles.includes('GYM_ADMIN'));
            }
        }
        return false;
    }

    joinActivity = () => {
        let formData = {};
        let URL = '';
        let segment_duration_max = null;
        let segment_duration_prev_max = null;
        if (this.state.workoutData.room_id) {
            URL = ApiURL._joinActivity
            formData = {
                'gym_id': this.state.workoutData.gym_id,
                'room_id': this.state.workoutData.room_id,
                'room_name': this.state.workoutData.room_name
            };
        } else {
            URL = ApiURL._joinActivityV3
            formData = {
                'activity_id': this.state.workoutData.activity_id
            };
        }
        axios.post(URL, formData).then(response => {
            console.log(response);
            let staffsInActivity = {};
            response.data.data.staffsInActivity.forEach(el => {
                let role = typeof el.roles === 'string' ? [el.roles] : el.roles;
                // if((checkRole('gym') && checkRole('gym',role)) || (!checkRole('gym') && !checkRole('gym',role))) {
                if (this.checkRole('gym',role)) {
                    return;
                }
                if (this.CheckValue(staffsInActivity[el.user_id])) {
                    staffsInActivity[el.user_id].socket_id.push(el.socket_id);
                    staffsInActivity[el.user_id].socket_id = this.StaffSort(staffsInActivity[el.user_id].socket_id);
                } else {
                    staffsInActivity[el.user_id] = this.setStaff(el, role);
                }
            });
        
            // ////console.log();
            this.callListners();
            if (response.data.data.playback_url && response.data.data.playback_url !== null
                && response.data.data.stream_key_arn !== null) {
                this.setState({
                    ivsStatus: true,
                    playBackURL: response.data.data.playback_url
                });
            }
            if (response.data.data.workout_builder && response.data.data.workout_builder.length) {
                let prevMax = null;
                response.data.data.workout_builder.forEach(el => {
                    let prevExerciseMax = null;
                    let ex_total_duration = 0;
                    let ex_prev_total_duration = 0;
                    el.exercises.forEach(ed => {
                        if (ex_total_duration) {
                            ex_prev_total_duration = ex_total_duration;
                        }
                        ex_total_duration = ed.exercise_duration_in_seconds + ex_total_duration;
                        if (ex_total_duration < el.segment_duration_in_seconds) {
                            ed['exercise_duration_in_seconds'] = ed.exercise_duration_in_seconds;
                        } else if (el.segment_duration_in_seconds > ex_prev_total_duration) {
                            ed['exercise_duration_in_seconds'] = el.segment_duration_in_seconds - ex_prev_total_duration;
                        } else {
                            ed['exercise_duration_in_seconds'] = 0;
                        }
                    });
                    el.exercises.forEach(ed => {
                        if (prevExerciseMax === null) {
                            ed['combinedExerciseMax'] = prevMax === null ?
                                ed.exercise_duration_in_seconds : ed.exercise_duration_in_seconds + prevMax;
                            prevExerciseMax = prevMax === null ?
                                ed.exercise_duration_in_seconds : ed.exercise_duration_in_seconds + prevMax;
                        } else {
                            ed['combinedExerciseMax'] = ed.exercise_duration_in_seconds + prevExerciseMax;
                            prevExerciseMax = ed.exercise_duration_in_seconds + prevExerciseMax;
                        }
                    });
                    if (prevMax === null) {
                        el['combinedMax'] = el.segment_duration_in_seconds;
                        prevMax = el.segment_duration_in_seconds;
                    } else {
                        el['combinedMax'] = el.segment_duration_in_seconds + prevMax;
                        prevMax = el.segment_duration_in_seconds + prevMax;
                    }
                    if (this.state.totalSeconds < el.combinedMax &&
                        segment_duration_max === null) {
                        el['values'] = [segment_duration_prev_max === null ? this.state.totalSeconds :
                            this.state.totalSeconds - (el.combinedMax - el.segment_duration_in_seconds)]
                        segment_duration_max = el.segment_duration_in_seconds;
                        this.setState({
                            segment_duration_max: el.segment_duration_in_seconds
                        });
                    } else if (this.state.totalSeconds > el.combinedMax) {
                        el['values'] = [el.segment_duration_in_seconds];
                        segment_duration_prev_max = el.segment_duration_in_seconds;
                        this.setState({
                            segment_duration_prev_max: el.segment_duration_in_seconds
                        });
                    }
                    else {
                        el['values'] = [0];
                    }
                    el['MIN'] = 0;
                    el['MAX'] = el.segment_duration_in_seconds;
                    el['STEP'] = 1;
                    el['width'] = Math.round(((el.segment_duration_in_seconds / response.data.data.total_activity_duration_in_seconds) * 100)) + '%';
                });
            }
            //console.log("response.data.data.workout_builder", response.data.data.workout_builder);
            const coachData = {
                first_name: response.data.data.first_name,
                last_name: response.data.data.last_name,
                created_by: response.data.data.created_by,
                profile_pic_thumb: response.data.data.profile_pic_thumb
            }
            this.setState({
                segmentDurations: [...response.data.data.workout_builder],
                total_activity_duration_in_seconds: response.data.data.total_activity_duration_in_seconds,
                coachData: { ...coachData },
                staffsInActivity: {...staffsInActivity}
            });
            setTimeout(() => {
                this.setState({
                    showSeekBar: true,
                    totalSeconds: this.state.totalSeconds + 1
                });
                this.startTimer();
            }, 200);

            if (response.data.data.usersInActivity && response.data.data.usersInActivity.length) {
                response.data.data.usersInActivity.forEach((element, index) => {
                    element['calories_burnt'] = element['calories_burnt'] ? Math.round(element['calories_burnt']) : 0;
                    element['showStream'] = false;
                    element['maxHeartRate'] = Math.round(208 - (0.7 * element['age']));
                    element['heartrate'] = Math.round(element['avg_heart_rate']);
                    if (element.video_stream && ((element.user_id === response.data.data.user_activity_data.user_id) ||
                        (element.participant_video_visibility_in_attempt === null || element.participant_video_visibility_in_attempt === 'ALL' ||
                            element.participant_video_visibility_in_attempt === 'FRIENDS_ONLY'))) {
                        element['video_stream'] = true;
                    } else {
                        element['video_stream'] = false;
                    }
                    if (element.user_status_in_activity === 'ENDED') {
                        element['color'] = '#808080';
                        element['opacity'] = '0.6';
                    } else if (element.user_status_in_activity === 'STARTED') {
                        element['color'] = colorSelector(element['avg_heart_rate'] ? Math.round((element['avg_heart_rate'] / element['maxHeartRate']) * 100) : 0);
                        element['opacity'] = 'unset';
                    }

                });
            }
            const obj = {};
            response.data.data.usersInActivity.forEach(element => {
                obj[element.user_id] = element;
            })
            const totalSeconds = response.data.data.started_at !== null ? Math.round(
                (new Date().getTime() - new Date(response.data.data.started_at).getTime())
                / 1000) : 0;
                console.log('---------------------------',localStorage.getItem('userType'));
            this.setState({
                allUsers: { ...obj },
                totalMembers: response.data.data.usersInActivity.length,
                started_at: response.data.data.started_at,
                userId: response.data.data.user_activity_data.user_id,
                userSessionId: response.data.data.user_activity_data.user_activity_session_id,
                userActivityData: { ...response.data.data.user_activity_data },
                gymName: response.data.data.gym_name,
                userType: localStorage.getItem('userType') || 'everyone'

            });
            console.log('---------------------------',this.state.userType);

            setTimeout(() => {
                this.state.socket.on("sync_tagged_videos", this.tagged_videos);
            }, 200);

            setTimeout(() => {
                this.syncSocketEmitHandler({
                    user_activity_session_id: this.state.ondemandData.user_activity_session_id,
                    web_socket_id: this.state.socket.id
                });
                this.setState({
                    firstTaggedEmit: true
                });
            }, 1500);
        }, error => {
            this.state.socket.disconnect();
            clearInterval(this.myInterval)
            localStorage.removeItem('workoutData');
            localStorage.removeItem('userType');
            localStorage.removeItem('accessToken');
            this.props.history.push('/Home');
        });

    }

    /* View in fullscreen */
    openFullscreen = () => {
        var elem = document.documentElement;
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.webkitRequestFullscreen) { /* Safari */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE11 */
            elem.msRequestFullscreen();
        }
        this.setState({
            fullScreen: true
        })

    }

    /* Close fullscreen */
    closeFullscreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) { /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE11 */
            document.msExitFullscreen();
        }
        this.setState({
            fullScreen: false
        })
    }

    toggleBox = (type) => {
        ////console.log(type);
        this.setState({
            toggleDiv: {
                ...this.state.toggleDiv,
                [type]: !this.state.toggleDiv[type]
            }
        })
    }

    messageReceived = (data) => {
        console.log("messageReceived", data);
        console.log("this.state.userSessionId", this.state.userSessionId);
        console.log("this.state.userSessionId", this.state.userId);
        if (!this.state.chatUxToggle && data.sender_id !== this.state.userId) {
            console.log("inside");
            this.setState({
                newMessageCount: this.state.newMessageCount + 1
            })
            localStorage.setItem("messageCount", this.state.newMessageCount);
        }
        const array = [...this.state.messageList];
        let messageFound = 0;
        array.forEach(el => {
            if (el.message_id === data.message_id) {
                el = { ...data }
                messageFound = 1;
            }
        });
        if (messageFound) {
            this.setState({
                messageList: [...array]
            })
        } else {
            this.setState({
                messageList: [{
                    ...data
                }, ...this.state.messageList]
            })
        }
    }

    sendMessage = () => {
        console.log(this.state.messageText);
        if (this.state.messageText === '') {
            return false;
        }
        const memberData = { ...this.state.allUsers[this.state.userId] };
        let messageBody = {
            sender_id: this.state.userId,
            receiver_id: this.state.coachData.created_by,
            activity_id: this.state.userActivityData.activity_id,
            user_activity_session_id: this.state.userSessionId,
            message: this.state.messageText.trim(),
            message_id: randomString(10) + moment().format().split("+")[0],
            date: new Date().getTime(),
            sender_name: memberData.first_name + ' ' + memberData.last_name,
            receiver_name: this.state.coachData.first_name + ' ' + this.state.coachData.last_name,
            sender_profile_pic: memberData.profile_pic_thumb
        };
        // console.log("messageBody", messageBody);
        if (this.state.userType === 'coach2') {
            console.log("messageBody", messageBody);
            this.state.socket.emit('message_to_coach', messageBody, (data) => {
                console.log("message_to_coach", data);
            });
        } else if (this.state.userType === 'owner') {
            console.log("messageBody", messageBody);
            messageBody['receiver_id'] = 'owner';
            messageBody['receiver_name'] = this.state.gymName
            messageBody['receiver_profile_pic'] = this.state.workoutData.logo_thumb
            this.state.socket.emit('message_to_owner', messageBody, (data) => {
                console.log("message_to_coach", data);
            });
        } else {
            delete messageBody.receiver_id;
            //console.log("All");
            console.log("messageBody", messageBody);
            this.state.socket.emit('message_to_all_members', messageBody, (data) => {
                console.log(data);
            });
        }

        this.setState({
            messageList: [{
                ...messageBody
            }, ...this.state.messageList]
        })
        setTimeout(() => {
            this.setState({
                messageText: ''
            })
        }, 100);
    }

    handleKeyPress = (e) => {
        //console.log(e.which);
        if ((e.which === 32 || e.which === 13) && !e.target.value.length) {
            e.preventDefault();
            return;
        }
        //console.log((e.which === 32 || e.which === 13) && !e.target.value.length);
        if (e.which === 13) {
            this.sendMessage();
        }
    }

    mesChangeHandler = (event) => {
        this.setState({
            messageText: event.target.value
        })
    }

    showDropdown = () => {
        this.setState({
            showChatDropdown: !this.state.showChatDropdown
        });
    }

    selectUser = (userType) => {
        this.setState({
            userType: userType,
            showChatDropdown: false
        });
        localStorage.setItem('userType',userType);
    }

    handleScroll = (e) => {
        // console.log("e.target.scrollHeight", e.target.scrollHeight);
        // console.log("e.target.clientHeight", e.target.clientHeight);
        // console.log("e.target.scrollTop", e.target.scrollTop);
        // console.log(e.target.scrollHeight - (e.target.clientHeight - e.target.scrollTop));
        let array = [];
        if (e.target.scrollHeight === (e.target.clientHeight - e.target.scrollTop) && this.state.hasMore) {
            // console.log("on Top");
            const obj = {
                'user_id': this.state.userId,
                'activity_id': this.state.userActivityData.activity_id,
                'offset': this.state.messageList.length
            }
            console.log(obj);
            this.state.socket.emit("get_chat", obj, (data) => {
                // console.log(data);
                if (data.length) {
                    array = data.map(el => JSON.parse(el));
                    console.log("getChat", array);
                    this.setState({
                        messageList: [...this.state.messageList, ...array]
                    });
                } else {
                    this.setState({
                        hasMore: false
                    });
                }
            });
        }
    }

    render() {
        console.log("this.state.segmentDurations", this.state.segmentDurations);
        // console.log("this.state.cur_exercise_data", this.state.cur_exercise_data);
        // console.log("cur_exercise_data", this.state.cur_exercise_data);
        // console.log("Message List", this.state.messageList.length);
        //console.log("this.state.segmentDurations", this.state.segmentDurations, this.state.total_activity_duration_in_seconds);
        // //console.log("this.state.values", this.state.values, this.state.showSeekBar, this.state.total_activity_duration_in_seconds);
        const rangeSliders = this.state.segmentDurations.map((el, index) =>
            <div key={index} style={{ width: 'calc(' + el.width + ' - 5px)', marginLeft: '3px' }}>
                <Range
                    disabled={true}
                    values={el.values}
                    step={el.STEP}
                    min={el.MIN}
                    max={el.MAX}
                    onChange={(values) => {
                        //console.log("values", values);
                        // this.setState({ values });
                        // this.setState({
                        //     totalSeconds: values[0]
                        // });
                        // seekVideoSubscriber.send(values[0]);
                    }}
                    renderTrack={({ props, children }) => (
                        <div
                            onMouseDown={props.onMouseDown}
                            onTouchStart={props.onTouchStart}
                            style={{
                                ...props.style,
                                height: "12px",
                                display: "flex",
                                width: "100%"
                            }}
                        >
                            <div
                                ref={props.ref}
                                style={{
                                    height: "8px",
                                    width: "100%",
                                    borderRadius: "8px",
                                    background: getTrackBackground({
                                        values: el.values,
                                        colors: ["#FF0042", "#868686"],
                                        min: el.MIN,
                                        max: el.MAX
                                    }),
                                    alignSelf: "center"
                                }}
                            >
                                {children}
                            </div>
                        </div>
                    )}
                    renderThumb={({ props, isDragged }) => (
                        <div
                            {...props}
                            style={{
                                ...props.style
                            }}
                        >
                            <div
                                style={{
                                    backgroundColor: isDragged ? "yellow" : "yellow"
                                }}
                            />
                        </div>
                    )}
                />
            </div>
        )
        const usersInActivity = Object.values(this.state.allUsers).map((el, index) =>
            <MemberList key={index} type="black" {...el} userId={this.state.userId}
                rank={index + 1} ondemand={false} startStream={() => this.startStream(el, 0)}
                endStream={() => this.endStream(el, 0)} />)
        const videos = Object.values(this.state.allUsers).filter(el =>
            el.video_stream && el.showStream).map(el =>
                <UserVideo ondemand={false} userID={this.state.userId} key={el.user_id} {...el} />);
        const messages = this.state.messageList.map((el, index) =>
            <div key={el.message_id + el.sender_id} style={{ flexDirection: this.state.userId === el.sender_id ? 'row' : 'row-reverse' }} className="messageBox">
                <div style={{ background: el.receiver_id && (el.receiver_id === this.state.userId) ? '#FF0042' : el.receiver_id && (el.receiver_id !== this.state.userId) ? '#FFFFFF' : (el.sender_id === this.state.userId) ? '#FFFFFF' : '#282828' }} className="message">
                    <span style={this.state.userId === el.sender_id ? { right: '9px' } : { left: '9px' }}>{el.receiver_id && (el.receiver_id === this.state.userId) ? '@Me' : el.receiver_id && (el.receiver_id !== this.state.userId) ? '@' + el.receiver_name : '@Everyone'}</span>
                    <p style={{ color: this.state.userId === el.sender_id ? '#0C0C0C' : '#FFFFFF' }}>{el.message}</p>
                    {this.state.userId !== el.sender_id ? <p className="senderName">{'~ ' + el.sender_name}</p> : null}
                </div>
                <img style={this.state.userId === el.sender_id ? { marginLeft: '5px' } : { marginRight: '5px' }}
                    src={(el.sender_profile_pic) ? el.sender_profile_pic : selectIcon('userDummy')} onError={imageError.bind(this)} />
            </div>);
        // const cur_segment_data = this.state.segmentDurations.length ? this.state.segmentDurations.find(el => this.state.totalSeconds <= el.combinedMax && ((el.combinedMax - el.segment_duration_in_seconds) <= this.state.totalSeconds)) : {};
        const remainingTime = this.state.cur_exercise_data !== undefined && Object.keys(this.state.cur_exercise_data).length
            ?
            this.state.cur_exercise_data.exercise_duration_in_seconds - (this.state.totalSeconds - (this.state.cur_exercise_data.combinedExerciseMax - this.state.cur_exercise_data.exercise_duration_in_seconds))
            : 0;
        // console.log(remainingTime);
        // console.log("====total seconds========", this.state.totalSeconds);
        // console.log("====segment_duration=====", this.state.cur_segment_data.segment_duration_in_seconds);
        // console.log("=========================", this.state.totalSeconds - (this.state.cur_segment_data.combinedMax - this.state.cur_segment_data.segment_duration_in_seconds));
        // console.log("====remaining time=======", remainingTime);
        return (
            <section className="Casting">
                {
                    <AmazonIVSWorkaround totalSeconds={this.state.totalSeconds} ondemand={false}
                        URL={this.state.playBackURL} ivsStatus={this.state.ivsStatus} />
                }
                <div className="infoBox">
                    <div className="infoImageBox">
                        <img className="infoImage" alt="placeholder" src={this.state.workoutData.logo_thumb} onError={imageError} />
                        {/* <p>{this.state.gymName}</p> */}
                    </div>
                    {this.state.showSeekBar ?
                        <div className="rangeSlider">
                            {this.state.segmentDurations.length
                                ?
                                <p className="segmentTime">{this.convertTime(this.state.totalSeconds)}</p>
                                : null}
                            <p className={this.state.segmentDurations.length ? "activityTime" : "activityTime1"}>
                                {this.state.segmentDurations.length
                                    ?
                                    this.convertTime(this.state.total_activity_duration_in_seconds)
                                    :
                                    this.convertTime(this.state.totalSeconds)
                                }
                            </p>
                            {this.state.totalSeconds < this.state.total_activity_duration_in_seconds &&
                                this.state.cur_exercise_data !== undefined ?
                                <p className="segmentName">{this.state.cur_exercise_data.exercise_name}</p> : null}
                            {rangeSliders}
                            <div>
                            </div>
                        </div> : null}
                    <div className="time">
                        {this.state.showSeekBar && this.state.segmentDurations.length
                            && this.state.totalSeconds < this.state.total_activity_duration_in_seconds &&
                            this.state.cur_exercise_data !== undefined ?
                            <CircularProgressbarWithChildren value={remainingTime} maxValue={this.state.cur_exercise_data.exercise_duration_in_seconds} strokeWidth={3}
                                styles={{ path: { stroke: '#FF0042' }, trail: { stroke: 'rgba(0,0,0,0.001)' } }}>
                                <div className={this.convertTime(remainingTime).length > 6 ? "newInnerCircle" : "innerCircle"}>
                                    {Object.keys(this.state.cur_segment_data).length ?
                                        <p style={{ textAlign: 'center' }}>
                                            {remainingTime > 59 ? this.convertTime(remainingTime) : remainingTime > 9 ? remainingTime : '0' + remainingTime}
                                        </p>
                                        : null}
                                </div>
                            </CircularProgressbarWithChildren> : null
                        }
                    </div>
                </div>
                <div className="membersBox"
                    style={{ justifyContent: videos.length ? 'space-between' : 'flex-end' }}>
                    {videos.length ?
                        <div className="videoContainer" style={{ position: 'relative' }}>
                            <button style={{ flexDirection: !this.state.toggleDiv.video ? 'row-reverse' : 'row' }}
                                className={this.state.toggleDiv.video ? 'hideVideoNew' : 'hideVideo'} onClick={() => this.toggleBox('video')}>
                                <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={
                                    !this.state.toggleDiv.video ? faChevronLeft : faChevronRight} />
                            </button>

                            <div className="videoSubContainer" style={{ opacity: this.state.toggleDiv.video ? 0 : 1 }}>
                                <div id="xyz-board" className="friendsBox">
                                    <div className="videoList">
                                        {videos}
                                    </div>
                                </div>
                            </div>

                        </div>

                        : null}
                    <div className="boardContainer" style={{ position: 'relative' }}>
                        {usersInActivity.length ?
                            <button style={{ flexDirection: !this.state.toggleDiv.list ? 'row' : 'row-reverse' }}
                                className={this.state.toggleDiv.list ? 'hideUnhidenew' : 'hideUnhide'} onClick={() => this.toggleBox('list')}>
                                <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={
                                    !this.state.toggleDiv.list ? faChevronRight : faChevronLeft} />
                            </button>
                            : null}

                        <div className="boardSubContainer" style={{
                            opacity: this.state.toggleDiv.list ? 0 : 1
                        }} >
                            <div className="toggleContainer">
                                <div onClick={this.chatToggle} className={this.state.chatUxToggle ? "membersCountSelected" : "membersCount"}>
                                    <img src={selectIcon('usersList')} />
                                    <p>{usersInActivity.length}</p>
                                </div>
                                <div onClick={this.chatToggle} className={this.state.chatUxToggle ? "chatIconSelected" : "chatIcon"}>
                                    <img src={selectIcon('chatIcon')} />
                                    {!this.state.chatUxToggle && this.state.newMessageCount > 0 ?
                                        <div className="messageCount">
                                            <p>{this.state.newMessageCount > 9 ? '9+' : this.state.newMessageCount}</p>
                                        </div> : null}
                                </div>
                            </div>
                            {this.state.chatUxToggle ?
                                <div className="chatListContainer">
                                    <div style={{
                                        opacity: this.state.showChatDropdown ? '0.6' : 'unset',
                                        background: this.state.showChatDropdown ? 'linear-gradient(180deg, rgba(66, 66, 66, 0.4) 0%, #424242 88.02%)' : '#424242'
                                    }} id="scrollableDiv" className="chatList">
                                        {messages}
                                    </div>
                                    <div className="chatBox">
                                        <input maxLength={140} value={this.state.messageText} placeholder="Type Message..." type="text"
                                            onKeyPress={this.handleKeyPress} onChange={this.mesChangeHandler} />
                                        <div className="selectedUser" onClick={this.showDropdown}>
                                            <img src={this.state.userType === 'owner' ? (this.state.workoutData.logo_thumb || null) : selectIcon(this.state.userType)} />
                                            {/* <img src={selectIcon(this.state.userType ? 'everyone' : 'everyone')} /> */}
                                        </div>
                                        <div className="sendButton" style={{ background: this.state.messageText.length ? '#FF0042' : 'grey' }}>
                                            <img onClick={this.sendMessage} src={selectIcon('sendMessage')} />
                                        </div>
                                        {this.state.showChatDropdown ?
                                            <div className="chatDropDown">
                                                <div onClick={() => this.selectUser('owner')} style={{ borderBottom: '1px solid #FFFFFF' }} className="selUserType">
                                                    <img src={this.state.workoutData.logo_thumb || null} />
                                                    <p>{this.state.gymName || ''}</p>
                                                </div>
                                                {this.state.staffsInActivity[this.state.coachData.created_by]?<div onClick={() => this.selectUser('coach2')} style={{ borderBottom: '1px solid #FFFFFF' }} className="selUserType">
                                                    <img src={selectIcon('coach2')} />
                                                    <p>Coach</p>
                                                </div>:null}
                                                {/* <div onClick={() => this.selectUser('coach2')} style={{ borderBottom: '1px solid #FFFFFF' }} className="selUserType">
                                                    <img src={selectIcon('coach2')} />
                                                    <p>Coach</p>
                                                </div> */}
                                                <div onClick={() => this.selectUser('everyone')} className="selUserType">
                                                    <img src={selectIcon('everyone')} />
                                                    <p>Everyone</p>
                                                </div>
                                            </div> : null}

                                    </div>
                                </div> :
                                <div className="memberListContainer">
                                    {usersInActivity}
                                </div>
                            }
                        </div>

                    </div>
                </div>
                <div className="statsContainer">
                    {this.state.toggleDiv.stats ?
                        <button style={{ flexDirection: 'row' }}
                            className='showStats' onClick={() => this.toggleBox('stats')}>
                            <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={faChevronUp} />
                        </button> : null
                    }
                    <div className="statsBox" style={{ opacity: this.state.toggleDiv.stats ? 0 : 1, transition: 'opacity 0.3s' }}>
                        <img className="statsImage" src={selectIcon('backgroundStats')} />
                        <button style={{ flexDirection: 'row', display: this.state.toggleDiv.stats ? 'none' : 'block' }}
                            className='hideStats' onClick={() => this.toggleBox('stats')}>
                            <FontAwesomeIcon style={{ color: '#FFFFFF' }} icon={faChevronDown} />
                        </button>
                        <UserStats {...this.state.selfData}
                            rank={(Object.keys(this.state.allUsers).indexOf(this.state.userId)) + 1} />
                    </div>
                    <div className="fullScreen">
                        <img className="fullScreenImage" src={selectIcon(this.state.fullScreen ? 'exitFullscreen' : 'fullScreen')} onClick={this.state.fullScreen ? this.closeFullscreen : this.openFullscreen} />
                        <FontAwesomeIcon className="audioIcon" onClick={this.toggle} style={{ color: '#FFFFFF' }} icon={
                            !this.state.mute ? faVolumeUp : faVolumeMute} />
                    </div>

                </div>
            </section>
        );
    }

}

export default Casting;