import React from 'react';
import './UserVideo.scss';
import { selectIcon, colorSelector } from '../../../components/UI/selector';

const UserVideo = (props) => {
    return (
        <div className="UserVideo" style={{ borderColor: colorSelector((props.heartrate / props.maxHeartRate) * 100) }}>
            {props.ondemand ? <video className="antVideo" src={props.ant_media_recording} autoPlay muted></video>
                :
                <video className="antVideo" id={props.user_id} autoPlay muted></video>
            }
            <div className="videoLabel" style={{ justifyContent: props.ondemand ? 'flex-start' : 'space-between' }}>
                <p>{props.userID === props.user_id ? 'Me' : props.first_name.length > 8 ? props.first_name.substring(0, 7) + '...' : props.first_name}
                </p>
                {props.ondemand ? null :
                    <div className="videoCalorieInfo">
                        <img className="videoCalorieIcon" alt="placeholder" src={selectIcon('caloriesIcon')} />
                        <p>{props.calories_burnt}</p>
                    </div>}
            </div>
        </div>
    );
};

export default UserVideo;