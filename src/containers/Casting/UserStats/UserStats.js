import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import './UserStats.scss';
import { selectIcon, textColorSelector } from '../../../components/UI/selector';
import { imageError } from '../../../services/subscriberService';

const UserStats = (props) => {
    return (
        <div className="UserStats">
            <div className="heartRateBox">
                <FontAwesomeIcon className="heartIcon" style={{ color: textColorSelector(Math.round(props.max_heart_rate_percent)) }}
                    icon={faHeart} />
                <div className="averageBox">
                    <p style={{ color: props.max_heart_rate_percent ? textColorSelector(Math.round(props.max_heart_rate_percent)) : 0 }}>
                        {props.current_heart_rate ? Math.round(props.current_heart_rate) : 0}
                    </p>
                    <span>Actual</span>
                </div>
                <div className="averageBox">
                    <p style={{ color: props.max_of_max_heart_rate_percent ? textColorSelector(Math.round(props.max_of_max_heart_rate_percent)) : 0 }}>
                        {props.max_heart_rate ? Math.round(props.max_heart_rate) : 0}
                    </p>
                    <span>Max</span>
                </div>
                <div className="averageBox">
                    <p style={{ color: props.avg_of_max_heart_rate_percent ? textColorSelector(Math.round(props.avg_of_max_heart_rate_percent)) : 0 }}>
                        {props.avg_heart_rate ? Math.round(props.avg_heart_rate) : 0}
                    </p>
                    <span>Avg</span>
                </div>
            </div>
            <div className="calorieBox">
                <img className="calorieBoxIcon" alt="placeholder" src={selectIcon('calorieIcon2x')} onError={imageError} />
                <p>{props.calories_burnt ? Math.round(props.calories_burnt) : 0}</p>
            </div>
            {/* <div className="calorieBox">
                <img className="rankIcon" alt="placeholder" src={selectIcon('rankIcon')} onError={imageError} />
                <p>{props.rank}</p>
            </div> */}
        </div>
    );
}

export default UserStats;