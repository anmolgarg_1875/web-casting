import React from 'react';
import './MemberStream.scss';
import { WebRTCAdaptor } from '../../../js/webrtc_adaptor';
import { imageError } from '../../../services/subscriberService';
import { env } from '../../../services/CommonFunction';

const PlayStream = (props) => {
    const userId = props.user_activity_session_id;
    var pc_config = null;

    var sdpConstraints = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    var mediaConstraints = {
        video: true,
        audio: true
    };
    // var WebRTCAdaptor = require(adapter);

    let webRTCAdaptor = new WebRTCAdaptor({
        websocket_url: env('AntMediaUrl'),
        mediaConstraints: mediaConstraints,
        peerconnection_config: pc_config,
        sdp_constraints: sdpConstraints,
        remoteVideoId: props.user_id,
        isPlayMode: true,
        callback: function (info) {
            // console.log(info);
            if (info === "initialized") {

                webRTCAdaptor.play(userId);
            } else if (info === "play_started") {
                //play_started
                //   console.log("play started");
            } else if (info === "play_finished") {
                // play finishedthe stream
                //   console.log("play finished");
            }
        },
        callbackError: function (error) {
            //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
            // console.log("error callback: " + error);
            // alert(error);
        }
    });
    return webRTCAdaptor;
}

export default PlayStream;