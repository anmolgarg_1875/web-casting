import React from 'react';
import './BoardUser.scss';
import { selectIcon } from '../../../components/UI/selector';
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';
import { imageError } from '../../../services/subscriberService';

const BoardUser = (props) => {
    return (
        <div className={props.userId === props.user_id ? "BoardUserWhite" : "BoardUserBlack"}
            style={{
                opacity: props.opacity === 'unset' ? 'unset' : '0.8',
                backgroundColor: props.userId === props.user_id ? '#FFFFFF' : props.opacity === 'unset' ? 'rgba(0,0,0,0.7)' : 'rgba(0,0,0,0.8)'
            }}>
            <div className="userInfo">
                <div className="userIndex">
                    <p>{props.rank}</p>
                    <div className="circularBarDiv" >
                        <CircularProgressbarWithChildren value={props.heartrate} maxValue={props.maxHeartRate} strokeWidth={5}
                            styles={{ path: { stroke: props.color }, trail: { stroke: props.userId === props.user_id ? '#FFFFFF' : '#ffffff00' } }}>
                            <img className="userImage" onError={imageError} style={{ borderColor: props.userId === props.user_id ? '#FFFFFF' : '#ffffff00' }}
                                alt="placeholder" src={props.profile_pic_thumb} />
                        </CircularProgressbarWithChildren>
                    </div>
                </div>
                <p>{props.userId === props.user_id ? 'Me' : props.first_name.length > 8 ? props.first_name.substring(0, 7) + '...' : props.first_name}
                    {/* {props.ondemand && props.total_attempts_yet?'x '+props.total_attempts_yet:''} */}
                </p>
                {props.video_stream && props.showStream ? <img onClick={props.endStream} className="cameraIcon" alt="placeholder" src={selectIcon('cameraFilled')} onError={imageError} /> : null}
                {props.userId !== props.user_id && props.video_stream && !props.showStream ? <img onClick={props.startStream} className="cameraIcon" alt="placeholder" src={selectIcon('cameraBlank')} onError={imageError} /> : null}
                {props.userId === props.user_id && props.video_stream && !props.showStream ? <img onClick={props.startStream} className="cameraIcon" style={{ height: '14px' }} alt="placeholder" src={selectIcon('redVideoIcon')} onError={imageError} /> : null}
            </div>
            <div className="calorieInfo">
                <img className="calorieIcon" alt="placeholder" src={selectIcon('calories')} onError={imageError} />
                <p>{props.calories_burnt}</p>
            </div>
        </div>
    );
}

export default BoardUser;