import React from 'react';
import './RangeSlider.scss';
import { Range, getTrackBackground } from "react-range";

const RangeSlider = (props) => {
    console.log("Range Slider", props);
    return (
        <Range
            disabled={true}
            values={props.values}
            step={props.step}
            min={props.MIN}
            max={props.MAX}
            // onChange={(values) => {
            //     console.log("values", values);
            //     this.setState({ values });
            //     this.setState({
            //         totalSeconds: values[0]
            //     });
            //     seekVideoSubscriber.send(values[0]);
            // }}
            renderTrack={({ props, children }) => (
                <div
                    onMouseDown={props.onMouseDown}
                    onTouchStart={props.onTouchStart}
                    style={{
                        ...props.style,
                        height: "12px",
                        display: "flex",
                        width: "100%"
                    }}
                >
                    <div
                        ref={props.ref}
                        style={{
                            height: "12px",
                            width: "100%",
                            borderRadius: "4px",
                            background: getTrackBackground({
                                values: props.values,
                                colors: ["#F22B5D", "#868686"],
                                min: props.MIN,
                                max: props.MAX
                            }),
                            alignSelf: "center"
                        }}
                    >
                        {children}
                    </div>
                </div>
            )}
            renderThumb={({ props, isDragged }) => (
                <div
                    {...props}
                    style={{
                        ...props.style,
                        // height: "25px",
                        // width: "25px",
                        outline: 'none',
                        borderRadius: "25px",
                        backgroundColor: "#F7F9F9",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        boxShadow: "0px 2px 6px #AAA"
                    }}
                >
                    <div
                        style={{
                            height: "1px",
                            width: "1px",
                            backgroundColor: isDragged ? "darkgrey" : "darkgrey"
                        }}
                    />
                </div>
            )}
        />)
};

export default RangeSlider;