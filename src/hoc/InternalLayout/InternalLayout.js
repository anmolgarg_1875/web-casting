import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Casting from '../../containers/Casting/Casting';
import Ondemand from '../../containers/Ondemand/Ondemand';
import './InternalLayout.scss';


class InternalLayout extends Component {

    componentDidMount() {
        // console.log(JSON.parse(localStorage.getItem('workoutData')));
        const data = JSON.parse(localStorage.getItem('workoutData'))
        if (this.props.history.location.pathname === "/") {
            if (data.is_ondemand_workout) {
                this.props.history.push('/Ondemand');
            } else {
                this.props.history.push('/Casting');
            }
        }
    }

    render() {
        return (
            <div>
                <Route path="/Casting" component={Casting} />
                <Route path="/Ondemand" component={Ondemand} />
            </div>
        );
    }
}

export default InternalLayout;