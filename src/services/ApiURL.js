const ApiURL = {
    _joinActivity: 'v2/member/join-activity',
    _joinActivityV3: 'v3/member/join-activity',
    _ondemandActivity: '/v1/activity/start-ondemand',
    _endOndemand: '/v1/activity/end-ondemand'
}

export default ApiURL;