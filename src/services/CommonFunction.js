const AntMediaUrl = {
    local: 'wss://antmedia.tribe.fitness/WebRTCAppEE/websocket',
    develop: 'wss://antmedia.tribe.fitness/WebRTCAppEE/websocket',
    staging: 'wss://antmedia.tribe.fitness/staging/websocket',
    production: 'wss://antmedia.tribe.fitness/prod/websocket'
    // develop: 'ws://antmedia.tribe.fitness:5080/WebRTCAppEE/websocket',
    // staging: 'ws://antmedia.tribe.fitness:5080/staging/websocket',
    // production: 'ws://antmedia.tribe.fitness:5080/prod/websocket'
}

const Urls = {
    local: 'https://tribe.fitness.ngrok.io/',
    develop: 'https://dev.api.tribe.fitness/',
    staging: 'https://staging.api.tribe.fitness/',
    production: 'https://api.tribe.fitness/'
    // production: 'https://api.tribe.fitness/'
}

const OnDemandSocket = {
    // develop: 'http://dev-service-318849498.us-east-1.elb.amazonaws.com?access_token=',
    // staging: 'http://staging-service-550164730.us-east-1.elb.amazonaws.com?access_token=',
    // production: 'http://prod-service-1399505486.us-east-1.elb.amazonaws.com?access_token='
    local: 'https://tribe.fitness.ngrok.io?access_token=',
    develop: 'https://dev.api.tribe.fitness?access_token=',
    staging: 'https://staging.api.tribe.fitness?access_token=',
    production: 'https://api.tribe.fitness?access_token='
}


export const env = (status, types) => {
    // const type = 'local';
    // const type = 'develop';
    // const type = 'staging';
    const type = 'production';
    return eval(status)[types || type]
}

export const randomString = (length) => {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}


